#include "Main.h"

int __stdcall DllMain(HINSTANCE hModule, DWORD dwReason, void* lpReserved)
{
	switch (dwReason)
	{
		case DLL_PROCESS_ATTACH:
		{
			Cube::OnPatch();
			ExtendedLevels::OnPatch();
			break;
		}
	}

	return TRUE;
}