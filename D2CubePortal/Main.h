#pragma once

#ifndef h_dllmain_
#define h_dllmain_

#define WIN32_LEAN_AND_MEAN
#define _CRT_SECURE_NO_DEPRECATE
#define _WIN32_WINNT 0x600

#include <windows.h>
#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <ctime>
#include <iosfwd>
#include <list>
#include <math.h>
#include <process.h>
#include <psapi.h>
#include <shlwapi.h>
#include <sstream>
#include <string>
#include <time.h>
#include <tlhelp32.h>
#include <valarray>
#include <vector>
#include <winbase.h>
#include <windef.h>
#include <winnt.h>
#include <winuser.h>
#include <queue>
#include <direct.h>	
#include <errno.h>
#include <io.h>
#include <tchar.h>
#include <iomanip>
#include <unordered_map>
#include <random>
#include <iterator>
#include <algorithm>
#include <map>
#include <fstream>
#include <regex>
#include <deque>
#include <random>
#include <mbstring.h>
#include <malloc.h>
#include <wchar.h>
#include <conio.h>
#include <cctype>
#include <memory>
#include <cstdarg>
#include <type_traits>

using namespace std;

static const DWORD DLLBASE_BNCLIENT = (DWORD)LoadLibraryA("Bnclient.dll");
static const DWORD DLLBASE_D2CLIENT = (DWORD)LoadLibraryA("D2Client.dll");
static const DWORD DLLBASE_D2CMP = (DWORD)LoadLibraryA("D2CMP.dll");
static const DWORD DLLBASE_D2COMMON = (DWORD)LoadLibraryA("D2Common.dll");
static const DWORD DLLBASE_D2DDRAW = (DWORD)LoadLibraryA("D2DDraw.dll");
static const DWORD DLLBASE_D2DIRECT3D = (DWORD)LoadLibraryA("D2Direct3D.dll");
static const DWORD DLLBASE_D2GAME = (DWORD)LoadLibraryA("D2Game.dll");
static const DWORD DLLBASE_D2GDI = (DWORD)LoadLibraryA("D2Gdi.dll");
static const DWORD DLLBASE_D2GFX = (DWORD)LoadLibraryA("D2Gfx.dll");
static const DWORD DLLBASE_D2LANG = (DWORD)LoadLibraryA("D2Lang.dll");
static const DWORD DLLBASE_D2LAUNCH = (DWORD)LoadLibraryA("D2Launch.dll");
static const DWORD DLLBASE_D2MCPCLIENT = (DWORD)LoadLibraryA("D2MCPClient.dll");
static const DWORD DLLBASE_D2MULTI = (DWORD)LoadLibraryA("D2Multi.dll");
static const DWORD DLLBASE_D2NET = (DWORD)LoadLibraryA("D2Net.dll");
static const DWORD DLLBASE_D2SOUND = (DWORD)LoadLibraryA("D2Sound.dll");
static const DWORD DLLBASE_D2WIN = (DWORD)LoadLibraryA("D2Win.dll");
static const DWORD DLLBASE_FOG = (DWORD)LoadLibraryA("Fog.dll");
static const DWORD DLLBASE_STORM = (DWORD)LoadLibraryA("Storm.dll");
static const DWORD DLLBASE_IJL11 = (DWORD)LoadLibraryA("ijl11.dll");
static const DWORD DLLBASE_BINKW32 = (DWORD)LoadLibraryA("binkw32.dll");
static const DWORD DLLBASE_SMACKW32 = (DWORD)LoadLibraryA("SmackW32.dll");

#define NAKED __declspec(naked)

#include "D2Constants.h"
#include "D2DataTables.h"
#include "D2Structs.h"
#include "D2Ptrs.h"
#include "D2TxtRecords.h"

#include "D2_RedPortal.h"
#include "Offset.h"
#include "D2_Cube.h"
#include "ExtendedLevels.h"

#define D2ERROR(format, ...) { ShowWindow(D2GFX_GetHwnd(),SW_HIDE);   ShowMsgBox(format, ##__VA_ARGS__); exit(-1); }
#define D2ASSERT(e, format, ...) if(!e) { ShowWindow(D2GFX_GetHwnd(),SW_HIDE);   ShowMsgBox(format, ##__VA_ARGS__); exit(-1); }

#endif