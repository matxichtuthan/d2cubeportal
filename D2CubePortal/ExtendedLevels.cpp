#include "Main.h"

DWORD __stdcall ExtendedLevels::GetActByLevelNo(DWORD nLevel)
{
	LevelsTxt* pLevel = TXT_GetLevelsRecord(nLevel);
	if (!pLevel) return 0;

	return pLevel->nAct;
}

BYTE __fastcall ExtendedLevels::GetActByLevel(Level* pLevel)
{
	D2ASSERT(pLevel, "Level pointer is null!")
	return (BYTE)GetActByLevelNo(pLevel->dwLevelNo);
}

BYTE __fastcall ExtendedLevels::GetActByRoom2(int _1, Room2* pRoom2)
{
	D2ASSERT(pRoom2, "Room pointer is null!")
	D2ASSERT(pRoom2->pLevel, "Level pointer is null!")
	return (BYTE)GetActByLevelNo(pRoom2->pLevel->dwLevelNo);
}

NAKED BYTE __fastcall ExtendedLevels::GetActByLevelNo_STUB1(DWORD nLevel)
{
	__asm
	{
		push edx
		push ecx
		push edi
		push ecx
		call ExtendedLevels::GetActByLevelNo
		pop edi
		pop ecx
		pop edx
		mov[esp + 13h + 4h], al
		ret
	}
}

NAKED BYTE __fastcall ExtendedLevels::GetActByLevelNo_STUB2(DWORD nLevel)
{
	__asm
	{
		push edx
		push ecx
		push edi
		push edx
		call ExtendedLevels::GetActByLevelNo
		pop edi
		pop ecx
		pop edx
		mov[esp + 13h + 4h], al
		ret
	}
}

void ExtendedLevels::OnPatch()
{
	Patch(CALL, "D2Common.dll", 0x37AA0, (DWORD)ExtendedLevels::GetActByLevel, 22, "Replace inline GetActByLevel"); //  Ordinal10447
	Patch(CALL, "D2Common.dll", 0x37B37, (DWORD)ExtendedLevels::GetActByLevel, 26, "Replace inline GetActByLevel"); //  Ordinal10135
	Patch(CALL, "D2Common.dll", 0x378D0, (DWORD)ExtendedLevels::GetActByRoom2, 22, "Replace inline GetActByLevel"); //  Ordinal11074

	Patch(CALL, "D2Common.dll", 0x17196, (DWORD)ExtendedLevels::GetActByLevelNo_STUB1, 20, "Replace inline GetActByLevel"); //  sub_6FD67180
	Patch(CALL, "D2Common.dll", 0x43620, (DWORD)ExtendedLevels::GetActByLevelNo_STUB1, 20, "Replace inline GetActByLevel"); //  sub_6FD93580
	Patch(CALL, "D2Common.dll", 0x4A6A0, (DWORD)ExtendedLevels::GetActByLevelNo_STUB2, 20, "Replace inline GetActByLevel"); //  sub_6FD9A670

	Patch(JUMP, "D2Common.dll", -10459, (DWORD)ExtendedLevels::GetActByLevelNo, 5, "Replace original ExtendedLevels::GetActByLevelNo");

	// The original value is 400
	Patch(CUSTOM, "D2Client.dll", 0x5F2EB + 1, 4096, 4, "Automap patch I");
	Patch(CUSTOM, "D2Client.dll", 0x6054B + 1, 4096, 4, "Automap patch I");
	Patch(CUSTOM, "D2Client.dll", 0x60584 + 1, 4096, 4, "Automap patch I");
	Patch(CUSTOM, "D2Client.dll", 0x60619 + 1, 4096, 4, "Automap patch I");
	Patch(CUSTOM, "D2Client.dll", 0x61EBA + 1, 4096, 4, "Automap patch I");

	Patch(CUSTOM, "D2Client.dll", 0x60516 + 2, 4256, 4, "Automap patch II - stack fix");
	Patch(CUSTOM, "D2Client.dll", 0x61E76 + 2, 4172, 4, "Automap patch II - stack fix");

	// The original value is 99
	Patch(CUSTOM, "D2Client.dll", 0x60536 + 1, 399, 4, "Automap patch III");
	Patch(CUSTOM, "D2Client.dll", 0x61EA9 + 1, 399, 4, "Automap patch III");

	// The original value is 400
	Patch(CUSTOM, "D2Client.dll", 0x6055B + 4, 4096, 4, "Automap patch IV");
	Patch(CUSTOM, "D2Client.dll", 0x61ECE + 4, 4096, 4, "Automap patch IV");
}