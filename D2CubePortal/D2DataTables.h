#pragma once

#ifndef _D2DATATABLES_H
#define _D2DATATABLES_H

#pragma pack(1)

struct BodyLocsTxt;
struct PropertiesTxt;
struct StatesTxt;
struct MonStatsTxt;
struct MissilesTxt;
struct SkillDescTxt;
struct SkillsTxt;
struct CharStatsTxt;
struct ItemStatCostTxt;
struct SetItemsTxt;
struct UniqueItemsTxt;
struct LevelsTxt;
struct DifficultyLevelsTxt;
struct ArenaTxt;
struct CubeMainTxt;
struct LvlMazeTxt;
struct ItemsTxt;
struct ItemsTxtStat;
struct CubeInputItem;
struct CubeOutputItem;
struct D2BooksTxt;
struct D2LvlWarpTXT;
struct D2NpcTxt;
struct MonsterTxt;
struct SuperUniqueTxt;
struct ObjectsTxt;
struct LevelTxt;
struct GemsTxt;
struct ItemTypesTxt;
struct TreasureClassTxt;
struct ItemText;

struct MonsterTxt
{
	BYTE _1[0x6];					//0x00
	WORD nLocaleTxtNo;				//0x06
	WORD flag;						//0x08
	WORD _1a;						//0x0A
	union
	{
		DWORD flag1;				//0x0C
		struct
		{
			BYTE flag1a;			//0x0C
			BYTE flag1b;			//0x0D
			BYTE flag1c[2];			//0x0E
		};
	};
	BYTE _2[0x22];					//0x10
	WORD velocity;					//0x32
	BYTE _2a[0x52];					//0x34
	WORD tcs[3][4];					//0x86
	BYTE _2b[0x52];					//0x9E
	wchar_t szDescriptor[0x3c];		//0xF0
	BYTE _3[0x1a0];					//0x12C
};

struct SuperUniqueTxt
{
	WORD nIndex;
	WORD nLocaleTxtNo;
	DWORD _1[9];
	BYTE _1a[4];
	WORD tcs[4];
};

struct D2NpcTxt
{
	DWORD dwNpc;						//0x00
	DWORD dwSellMult;					//0x04
	DWORD dwBuyMult;					//0x08
	DWORD dwRepMult;					//0x0C
	DWORD dwQuestflagA;					//0x10
	DWORD dwQuestFlagB;					//0x14
	DWORD dwQuestFlagC;					//0x18
	DWORD dwQuestSellMultA;				//0x1C
	DWORD dwQuestSellMultB;				//0x20
	DWORD dwQuestSellMultC;				//0x24
	DWORD dwQuestBuyMultA;				//0x28
	DWORD dwQuestBuyMultB;				//0x2C
	DWORD dwQuestBuyMultC;				//0x30
	DWORD dwQuestRepMultA;				//0x34
	DWORD dwQuestRepMultB;				//0x38
	DWORD dwQuestRepMultC;				//0x3C
	DWORD dwMaxBuy;						//0x40
	DWORD dwMaxBuy_N;					//0x44
	DWORD dwMaxBuy_H;					//0x48
};

struct D2LvlWarpTXT
{
   DWORD dwLevelId;               //0x00
   DWORD dwSelectX;               //0x04
   DWORD dwSelectY;               //0x08
   DWORD dwSelectDX;               //0x0C
   DWORD dwSelectDY;               //0x10
   DWORD dwExitWalkX;               //0x14
   DWORD dwExitWalkY;               //0x18
   DWORD dwOffsetX;               //0x1C
   DWORD dwOffsetY;               //0x20
   DWORD dwLitVersion;               //0x24
   DWORD dwTiles;                  //0x28
   DWORD dwDirection;               //0x30
};

struct D2BooksTxt
{
   WORD wStr;                     //0x00
   WORD wSpellIcon;               //0x02
   DWORD dwPSpell;                  //0x04
   DWORD dwScrollSkillId;            //0x08
   DWORD dwBookSkillId;            //0x0C
   DWORD dwBaseCost;               //0x10
   DWORD dwCostPerCharge;            //0x14
   DWORD dwScrollICode;            //0x18
   DWORD dwBookICode;               //0x1C
};

struct ItemsTxtStat
{
	DWORD dwProp;                 //0x00
	DWORD dwPar;                  //0x04
	int dwMin;					  //0x08
	int dwMax;			          //0x0C
};

struct D2PropertyStrc
{
	DWORD nProperty;	//0x00
	int nLayer;		//0x04
	int nMin;		//0x08
	int nMax;		//0x0C
};

struct BodyLocsTxt
{
   union
   {
	  DWORD dwCode;               //0x00
	  char szCode[4];               //0x00
   };
};

struct PropertiesTxt
{
	WORD wProp;                   //0x00
	BYTE nSet[8];                 //0x02
	WORD wVal[7];                 //0x0A
	BYTE nFunc[8];                //0x18
	WORD wStat[7];                //0x20
};

struct StatesTxt
{
	WORD wState;					//0x00
	WORD wOverlay[4];               //0x02
	WORD wCastOverlay;              //0x0A
	WORD wRemoveOverlay;            //0x0C
	WORD wPrgOverlay;               //0x0E
	DWORD dwStateFlags;				//0x10
	DWORD dwStateFlagsEx;			//0x14
	WORD wStat;                     //0x18
	WORD wSetFunc;                  //0x1A
	WORD wRemFunc;                  //0x1C
	WORD wGroup;					//0x1E
	BYTE nColorPri;                 //0x20
	BYTE nColorShift;               //0x21
	BYTE nLightRGB[4];              //0x22
	WORD wOnSound;                  //0x26
	WORD wOffSound;                 //0x28
	WORD wItemType;                 //0x2A
	BYTE nItemTrans;				//0x2C
	BYTE nGfxType;                  //0x2D
	WORD wGfxClass;                 //0x2E
	WORD wCltEvent;                 //0x30
	WORD wCltEventFunc;             //0x32
	WORD wCltActiveFunc;            //0x34
	WORD wSrvActiveFunc;            //0x36
	WORD wSkill;					//0x38
	WORD wMissile;                  //0x3A
};

struct MonStatsTxt // size 0x1A8
{
	WORD	wId;						//0x00
	WORD	wBaseId;					//0x02
	WORD	wNextInClass;				//0x04
	WORD	wNameStr;					//0x06
	WORD	wDescStr;					//0x08
	WORD	_1a;						//0x0A
	//struct 
	//{
	//	BYTE	bisSpawn : 1;						//1
	//	BYTE	bisMelee : 1;						//2
	//	BYTE	bnoRatio : 1;						//3
	//	BYTE	bOpenDoors : 1;					//4
	//	BYTE	bSetBoss : 1;						//5
	//	BYTE	bBossXfer : 1;					//6
	//	BYTE	bBoss : 1;						//7
	//	BYTE	bPrimeEvil : 1;					//8
	//	BYTE	bNPC : 1;							//9
	//	BYTE	bInteract : 1;					//10
	//	BYTE	binTown : 1;						//11
	//	BYTE	blUndead : 1;						//12
	//	BYTE	bhUndead : 1;						//13
	//	BYTE	bDemon : 1;						//14
	//	BYTE	bFlying : 1;						//15
	//	BYTE	bKillable : 1;					//16
	//	BYTE	bSwitchAI : 1;					//17
	//	BYTE	bNoMultiShot : 1;					//18
	//	BYTE	bNeverCount : 1;					//19
	//	BYTE	bPetIgnore : 1;					//20
	//	BYTE	bDeathDmg : 1;					//21
	//	BYTE	bGenericSpawn : 1;				//22
	//	BYTE	bZoo : 1;							//23
	//	BYTE	bPlaceSpawn : 1;					//24
	//	BYTE	bInventory : 1;					//25
	//	BYTE	bEnabled : 1;						//26
	//	BYTE	bNoShldBlock : 1;					//27
	//	BYTE	bNoAura : 1;						//28
	//	BYTE	bRangedType : 1;					//29
	//} dwFlags;							//0x0C
	DWORD	dwFlags;					//0x0C
	DWORD	dwCode;						//0x10
	WORD	wMonSound;					//0x14
	WORD	wUMonSound;					//0x16
	WORD	wMonStatsEx;				//0x18
	WORD	wMonProp;					//0x1A
	WORD	wMonType;					//0x1C
	WORD	wAI;						//0x1E
	WORD	wSpawn;						//0x20
	BYTE	bSpawnX;					//0x22
	BYTE	bSpawnY;					//0x23
	WORD	bSpawnMode;					//0x24
	WORD	wMinion1;					//0x26
	WORD	wMinion2;					//0x28
	WORD	_1;							//0x2A
	BYTE	bPartyMin;					//0x2C
	BYTE	bPartyMax;					//0x2D
	BYTE	bRarity;					//0x2E
	BYTE	bMinGrp;					//0x2F
	BYTE	bMaxGrp;					//0x30
	BYTE	bSparsePopulate;			//0x31
	WORD	wVelocity;					//0x32
	WORD	wRun;						//0x34
	WORD	_2;							//0x36
	WORD	_2b;						//0x38
	WORD	wMissA1;					//0x3A
	WORD	wMissA2;					//0x3C
	WORD	wMissS1;					//0x3E
	WORD	wMissS2;					//0x40
	WORD	wMissS3;					//0x42
	WORD	wMissS4;					//0x44
	WORD	wMissC;						//0x46
	WORD	wMissSQ;					//0x48
	WORD	_3;							//0x4A
	BYTE	bAlign;						//0x4C
	BYTE	bTransLvl;					//0x4D
	BYTE	bThreat;					//0x4E
	BYTE	bAIdel;						//0x4F
	BYTE	bAIdel_N;					//0x50
	BYTE	bAIdel_H;					//0x51
	BYTE	bAiDist;					//0x52
	BYTE	bAiDist_N;					//0x53
	WORD	bAiDist_H;					//0x54
	WORD	wAiP1;						//0x56
	WORD	wAiP1_N;					//0x58
	WORD	wAiP1_H;					//0x5A
	WORD	wAiP2;						//0x5C
	WORD	wAiP2_N;					//0x5E
	WORD	wAiP2_H;					//0x60
	WORD	wAiP3;						//0x62
	WORD	wAiP3_N;					//0x64
	WORD	wAiP3_H;					//0x66
	WORD	wAiP4;						//0x68
	WORD	wAiP4_N;					//0x6A
	WORD	wAiP4_H;					//0x6C
	WORD	wAiP5;						//0x6E
	WORD	wAiP5_N;					//0x70
	WORD	wAiP5_H;					//0x72
	WORD	wAiP6;						//0x74
	WORD	wAiP6_N;					//0x76
	WORD	wAiP6_H;					//0x78
	WORD	wAiP7;						//0x7A
	WORD	wAiP7_N;					//0x7C
	WORD	wAiP7_H;					//0x7E
	WORD	wAiP8;						//0x80
	WORD	wAiP8_N;					//0x82
	WORD	wAiP8_H;					//0x84
	WORD	wTreasureClass1;			//0x86
	WORD	wTreasureClass2;			//0x88
	WORD	wTreasureClass3;			//0x8A
	WORD	wTreasureClass4;			//0x8C
	WORD	wTreasureClass1_N;			//0x8E
	WORD	wTreasureClass2_N;			//0x90
	WORD	wTreasureClass3_N;			//0x92
	WORD	wTreasureClass4_N;			//0x94
	WORD	wTreasureClass1_H;			//0x96
	WORD	wTreasureClass2_H;			//0x98
	WORD	wTreasureClass3_H;			//0x9A
	WORD	wTreasureClass4_H;			//0x9C
	BYTE	bTCQuestId;					//0x9E
	BYTE	bTCQuestCP;					//0x9F
	BYTE	bDrain;						//0xA0
	BYTE	bDrain_N;					//0xA1
	BYTE	bDrain_H;					//0xA2
	BYTE	bToBlock;					//0xA3
	BYTE	bToBlock_N;					//0xA4
	BYTE	bToBlock_H;					//0xA5
	WORD	bCrit;						//0xA6
	WORD	wSkillDamage;				//0xA8
	WORD	wLevel;						//0xAA
	WORD	wLevel_N;					//0xAC
	WORD	wLevel_H;					//0xAE
	union
	{
		struct
		{
			WORD	wMinHP;						//0xB0
			WORD	wMinHP_N;					//0xB2
			WORD	wMinHP_H;					//0xB4
		};
		WORD  MinHP[3];
	};
	union
	{
		struct
		{
			WORD	wMaxHP;						//0xB6
			WORD	wMaxHP_N;					//0xB8
			WORD	wMaxHP_H;					//0xBA
		};
		WORD  MaxHP[3];
	};
	WORD	wAC;						//0xBC
	WORD	wAC_N;						//0xBE
	WORD	wAC_H;						//0xC0
	WORD	wA1TH;						//0xC2
	WORD	wA1TH_N;					//0xC4
	WORD	wA1TH_H;					//0xC6
	WORD	wA2TH;						//0xC8
	WORD	wA2TH_N;					//0xCA
	WORD	wA2TH_H;					//0xCC
	WORD	wS1TH;						//0xCE
	WORD	wS1TH_N;					//0xD0
	WORD	wS1TH_H;					//0xD2
	WORD	wExp;						//0xD4
	WORD	wExp_N;						//0xD6
	WORD	wExp_H;						//0xD8
	WORD	wA1MinD;					//0xDA
	WORD	wA1MinD_N;					//0xDC
	WORD	wA1MinD_H;					//0xDE
	WORD	wA1MaxD;					//0xE0
	WORD	wA1MaxD_N;					//0xE2
	WORD	wA1MaxD_H;					//0xE4
	WORD	wA2MinD;					//0xE6
	WORD	wA2MinD_N;					//0xE8
	WORD	wA2MinD_H;					//0xEA
	WORD	wA2MaxD;					//0xEC
	WORD	wA2MaxD_N;					//0xEE
	WORD	wA2MaxD_H;					//0xF0
	WORD	wS1MinD;					//0xF2
	WORD	wS1MinD_N;					//0xF4
	WORD	wS1MinD_H;					//0xF6
	WORD	wS1MaxD;					//0xF8
	WORD	wS1MaxD_N;					//0xFA
	WORD	wS1MaxD_H;					//0xFC
	BYTE	bEl1Mode;					//0xFE
	BYTE	bEl2Mode;					//0xFF
	BYTE	bEl3Mode;					//0x100
	BYTE	bEl1Type;					//0x101
	BYTE	bEl2Type;					//0x102
	BYTE	bEl3Type;					//0x103
	BYTE	bEl1Pct;					//0x104
	BYTE	bEl1Pct_N;					//0x105
	BYTE	bEl1Pct_H;					//0x106
	BYTE	bEl2Pct;					//0x107
	BYTE	bEl2Pct_N;					//0x108
	BYTE	bEl2Pct_H;					//0x109
	BYTE	bEl3Pct;					//0x10A
	BYTE	bEl3Pct_N;					//0x10B
	BYTE	bEl3Pct_H;					//0x10C
	BYTE	_4;							//0x10D
	WORD	wEl1MinD;					//0x10E
	WORD	wEl1MinD_N;					//0x110
	WORD	wEl1MinD_H;					//0x112
	WORD	wEl2MinD;					//0x114
	WORD	wEl2MinD_N;					//0x116
	WORD	wEl2MinD_H;					//0x118
	WORD	wEl3MinD;					//0x11A
	WORD	wEl3MinD_N;					//0x11C
	WORD	wEl3MinD_H;					//0x11E
	WORD	wEl1MaxD;					//0x120
	WORD	wEl1MaxD_N;					//0x122
	WORD	wEl1MaxD_H;					//0x124
	WORD	wEl2MaxD;					//0x126
	WORD	wEl2MaxD_N;					//0x128
	WORD	wEl2MaxD_H;					//0x12A
	WORD	wEl3MaxD;					//0x12C
	WORD	wEl3MaxD_N;					//0x12E
	WORD	wEl3MaxD_H;					//0x130
	WORD	wEl1Dur;					//0x132
	WORD	wEl1Dur_N;					//0x134
	WORD	wEl1Dur_H;					//0x136
	WORD	wEl2Dur;					//0x138
	WORD	wEl2Dur_N;					//0x13A
	WORD	wEl2Dur_H;					//0x13C
	WORD	wEl3Dur;					//0x13E
	WORD	wEl3Dur_N;					//0x140
	WORD	wEl3Dur_H;					//0x142
	WORD	wResDmg;					//0x144
	WORD	wResDmg_N;					//0x146
	WORD	wResDmg_H;					//0x148
	WORD	wResMagic;					//0x14A
	WORD	wResMagic_N;				//0x14C
	WORD	wResMagic_H;				//0x14E
	WORD	wResFire;					//0x150
	WORD	wResFire_N;					//0x152
	WORD	wResFire_H;					//0x154
	WORD	wResLight;					//0x156
	WORD	wResLight_N;				//0x158
	WORD	wResLight_H;				//0x15A
	WORD	wResCold;					//0x15C
	WORD	wResCold_N;					//0x15E
	WORD	wResCold_H;					//0x160
	WORD	wResPoison;					//0x162
	WORD	wResPoison_N;				//0x164
	WORD	wResPoiosn_H;				//0x166
	BYTE	bColdEffect;				//0x168
	BYTE	bColdEffect_N;				//0x169
	WORD	bColdEffect_H;				//0x16A
	DWORD	dwSendSkills;				//0x16C
	WORD	wSkill1;					//0x170
	WORD	wSkill2;					//0x172
	WORD	wSkill3;					//0x174
	WORD	wSkill4;					//0x176
	WORD	wSkill5;					//0x178
	WORD	wSkill6;					//0x17A
	WORD	wSkill7;					//0x17C
	WORD	wSkill8;					//0x17E
	DWORD   _5[6];						//0x180
	BYTE	bSk1lvl;					//0x198
	BYTE	bSk2lvl;					//0x199
	BYTE	bSk3lvl;					//0x19A
	BYTE	bSk4lvl;					//0x19B
	BYTE	bSk5lvl;					//0x19C
	BYTE	bSk6lvl;					//0x19D
	BYTE	bSk7lvl;					//0x19E
	BYTE	bSk8lvl;					//0x19F
	DWORD	dwDamageRegen;				//0x1A0
	BYTE	bSplEndDeath;				//0x1A4
	BYTE	bSplGetModeChart;			//0x1A5
	BYTE	bSplEndGeneric;				//0x1A6
	BYTE	bSplClientEnd;				//0x1A7
};

struct MissilesTxt
{
   DWORD dwId;                     //0x00
   DWORD dwMissileFlags;            //0x04
   WORD wCltDoFunc;               //0x08
   WORD wCltHitFunc;               //0x0A
   WORD wSrvDoFunc;               //0x0C
   WORD wSrvHitFunc;               //0x0E
   WORD wSrvDmgFunc;               //0x10
   WORD wTravelSound;               //0x12
   WORD wHitSound;                  //0x14
   WORD wExplosionMissile;            //0x16
   WORD wSubMissile[3];            //0x18
   WORD wCltSubMissile[3];            //0x1E
   WORD wHitSubMissile[4];            //0x24
   WORD wCltHitSubMissile[4];         //0x2C
   WORD wProgSound;               //0x34
   WORD wProgOverlay;               //0x36
   DWORD dwParam[5];               //0x38
   DWORD dwHitPar[3];               //0x4C
   DWORD dwCltParam[5];            //0x58
   DWORD dwCltHitPar[3];            //0x6C
   DWORD dwDmgParam[2];            //0x78
   DWORD dwSrvCalc;               //0x80
   DWORD dwCltCalc;               //0x84
   DWORD dwHitCalc;               //0x88
   DWORD dwCltHitCalc;               //0x8C
   DWORD dwDmgCalc;               //0x90
   WORD wHitClass;                  //0x94
   WORD wRange;                  //0x96
   WORD wLevRange;                  //0x98
   BYTE nVel;                     //0x9A
   BYTE nVelLev;                  //0x9B
   WORD wMaxVel;                  //0x9C
   WORD wAccel;                  //0x9E
   WORD wAnimRate;                  //0xA0
   WORD wXoffset;                  //0xA2
   WORD wYoffset;                  //0xA4
   WORD wZoffset;                  //0xA6
   DWORD dwHitFlags;               //0xA8
   WORD wResultFlags;               //0xAC
   WORD wKnockBack;               //0xAE
   DWORD dwMinDamage;               //0xB0
   DWORD dwMaxDamage;               //0xB4
   DWORD dwMinLevDam[5];            //0xB8
   DWORD dwMaxLevDam[5];            //0xCC
   DWORD dwDmgSymPerCalc;            //0xE0
   DWORD dwElemType;               //0xE4
   DWORD dwElemMin;               //0xE8
   DWORD dwElemMax;               //0xEC
   DWORD dwMinElemLev[5];            //0xF0
   DWORD dwMaxElemLev[5];            //0x104
   DWORD dwElemDmgSymPerCalc;         //0x118
   DWORD dwElemLen;               //0x11C
   DWORD dwElemLevLen[3];            //0x120
   BYTE unk0x12C;                  //0x12C
   BYTE nSrcDamage;               //0x12D
   BYTE nSrcMissDmg;               //0x12E
   BYTE nHoly;                     //0x12F
   BYTE nLight;                  //0x130
   BYTE nFlicker;                  //0x131
   BYTE nRGB[3];                  //0x132
   BYTE nInitSteps;               //0x135
   BYTE nActivate;                  //0x136
   BYTE nLoopAnim;                  //0x137
   char szCelFile[64];               //0x138
   DWORD dwAnimLen;               //0x178
   DWORD dwRandStart;               //0x17C
   BYTE nSubLoop;                  //0x180
   BYTE nSubStart;                  //0x181
   BYTE nSubStop;                  //0x182
   BYTE nCollideType;               //0x183
   BYTE nCollision;               //0x184
   BYTE nClientCol;               //0x185
   BYTE nCollideKill;               //0x186
   BYTE nCollideFriend;            //0x187
   BYTE nNextHit;                  //0x188
   BYTE nNextDelay;               //0x189
   BYTE nSize;                     //0x18A
   BYTE nToHit;                  //0x18B
   BYTE nAlwaysExplode;            //0x18C
   BYTE nTrans;                  //0x18D
   WORD wQty;                     //0x18E
   DWORD dwSpecialSetup;            //0x190
   WORD wSkill;                  //0x194
   BYTE nHitShift;                  //0x196
   BYTE unk0x197[5];               //0x197
   DWORD dwDamageRate;               //0x19C
   BYTE nNumDirections;            //0x1A0
   BYTE nAnimSpeed;               //0x1A1
   BYTE nLocalBlood;               //0x1A2
   BYTE unk;                     //0x1A3
};

struct SkillsTxt
{
	DWORD dwSkillId; 					//0x00
	union
	{
		unsigned long long int qwSkillFlags;
		DWORD dwFlags[2];					//0x04
		struct
		{
			unsigned int vinterrupt : 1;
			unsigned int vleftskill : 1;
			unsigned int vItemTgtDo : 1;
			unsigned int vAttackNoMana : 1;
			unsigned int vTargetItem : 1;
			unsigned int vTargetAlly : 1;
			unsigned int vTargetPet : 1;
			unsigned int vTargetCorpse : 1;
			unsigned int vSearchOpenXY : 1;
			unsigned int vSearchEnemyNear : 1;
			unsigned int vSearchEnemyXY : 1;
			unsigned int vTargetableOnly : 1;
			unsigned int vUseAttackRate : 1;
			unsigned int vdurability : 1;
			unsigned int venhanceable : 1;
			unsigned int vnoammo : 1;
			unsigned int vimmediate : 1;
			unsigned int vweaponsnd : 1;
			unsigned int vstsounddelay : 1;
			unsigned int vstsuccessonly : 1;
			unsigned int vrepeat : 1;
			unsigned int vInGame : 1;
			unsigned int vKick : 1;
			unsigned int vInTown : 1;
			unsigned int vprgstack : 1;
			unsigned int vperiodic : 1;
			unsigned int vaura : 1;
			unsigned int vpassive : 1;
			unsigned int vfinishing : 1;
			unsigned int vprogressive : 1;
			unsigned int vlob : 1;
			unsigned int vdecquant : 1;
			unsigned int iPadding2 : 25;
			unsigned int vwarp : 1;
			unsigned int vusemanaondo : 1;
			unsigned int vscroll : 1;
			unsigned int vgeneral : 1;
			unsigned int vItemCltCheckStart : 1;
			unsigned int vItemCheckStart : 1;
			unsigned int vTgtPlaceCheck : 1;
		} dwFlag;
	};
	DWORD dwClassId;					//0x0C
	BYTE nAnim;							//0x10
	BYTE nMonAnim;						//0x11
	BYTE nSeqTrans;						//0x12
	BYTE nSeqNum;						//0x13
	BYTE nRange;						//0x14
	BYTE nSelectProc;					//0x15
	BYTE nSeqInput;						//0x16
	BYTE unk0x17;						//0x17
	WORD wITypeA1;						//0x18
	WORD wITypeA2;						//0x1A
	WORD wITypeA3;						//0x1C
	WORD wITypeB1;						//0x1E
	WORD wITypeB2;						//0x20
	WORD wITypeB3;						//0x22
	WORD wETypeA1;						//0x24
	WORD wETypeA2;						//0x26
	WORD wETypeB1;						//0x28
	WORD wETypeB2;						//0x2A
	WORD wSrvStartFunc;					//0x2C
	WORD wSrvDoFunc;					//0x2E
	WORD wSrvPrgFunc1;					//0x30
	WORD wSrvPrgFunc2;					//0x32
	WORD wSrvPrgFunc3;					//0x34
	WORD unk0x36;						//0x36
	DWORD dwPrgCalc1;					//0x38
	DWORD dwPrgCalc2;					//0x3C
	DWORD dwPrgCalc3;					//0x40
	WORD wPrgDamage;					//0x44
	WORD wSrvMissile;					//0x46
	WORD wSrvMissileA;					//0x48
	WORD wSrvMissileB;					//0x4A
	WORD wSrvMissileC;					//0x4C
	WORD wSrvOverlay;					//0x4E
	DWORD dwAuraFilter;					//0x50
	WORD nAuraStat[6];					//0x54
	DWORD dwAuraLenCalc;				//0x60
	DWORD dwAuraRangeCalc;				//0x64
	DWORD dwAuraStatCalc[6];			//0x68
	WORD wAuraState;					//0x80
	WORD wAuraTargetState;				//0x82
	WORD wAuraEvent[3];					//0x84
	WORD wAuraEventFunc[3];				//0x8A
	WORD wAuraTgtEvent;					//0x90
	WORD wAuraTgtEventFunc;				//0x92
	WORD wPassiveState;					//0x94
	WORD wPassiveIType;					//0x96
	WORD wPassiveStat[5];				//0x98
	WORD unk0xA2;						//0xA2
	DWORD dwPassiveCalc[5];				//0xA4
	WORD wPassiveEvent;					//0xB8
	WORD wPassiveEventFunc;				//0xBA
	WORD wSummon;						//0xBC
	BYTE nPetType;						//0xBE
	BYTE nSumMode;						//0xBF
	DWORD dwPetMax;						//0xC0
	WORD wSumSkill1;					//0xC4
	WORD wSumSkill2;					//0xC6
	WORD wSumSkill3;					//0xC8
	WORD wSumSkill4;					//0xCA
	WORD wSumSkill5;					//0xCC
	WORD unk0xCE;						//0xCE
	DWORD dwSumSkCalc1;					//0xD0
	DWORD dwSumSkCalc2;					//0xD4
	DWORD dwSumSkCalc3;					//0xD8
	DWORD dwSumSkCalc4;					//0xDC
	DWORD dwSumSkCalc5;					//0xE0
	WORD wSumUMod;						//0xE4
	WORD wSumOverlay;					//0xE6
	WORD wCltMissile;					//0xE8
	WORD wCltMissileA;					//0xEA
	WORD wCltMissileB;					//0xEC
	WORD wCltMissileC;					//0xEE
	WORD wCltMissileD;					//0xF0
	WORD wCltStFunc;					//0xF2
	WORD wCltDoFunc;					//0xF4
	WORD wCltPrgFunc1;					//0xF6
	WORD wCltPrgFunc2;					//0xF8
	WORD wCltPrgFunc3;					//0xFA
	WORD wStSound;						//0xFC
	WORD unk0xFE;						//0xFE
	WORD wDoSound;						//0x100
	WORD wDoSoundA;						//0x102
	WORD wDoSoundB;						//0x104
	WORD wCastOverlay;					//0x106
	WORD wTgtOverlay;					//0x108
	WORD wTgtSound;						//0x10A
	WORD wPrgOverlay;					//0x10C
	WORD wPrgSound;						//0x10E
	WORD wCltOverlayA;					//0x110
	WORD wCltOverlayB;					//0x112
	DWORD dwCltCalc1;					//0x114
	DWORD dwCltCalc2;					//0x118
	DWORD dwCltCalc3;					//0x11C
	WORD wItemTarget;					//0x120
	WORD wItemCastSound;				//0x122
	DWORD wItemCastOverlay;				//0x124
	DWORD dwPerDelay;					//0x128
	WORD wMaxLvl;						//0x12C
	WORD wResultFlags;					//0x12E
	DWORD dwHitFlags;					//0x130
	DWORD dwHitClass;					//0x134
	DWORD dwCalc1;						//0x138
	DWORD dwCalc2;						//0x13C
	DWORD dwCalc3;						//0x140
	DWORD dwCalc4;						//0x144
	DWORD dwParam1;						//0x148
	DWORD dwParam2;						//0x14C
	DWORD dwParam3;						//0x150
	DWORD dwParam4;						//0x154
	DWORD dwParam5;						//0x158
	DWORD dwParam6;						//0x15C
	DWORD dwParam7;						//0x160
	DWORD dwParam8;						//0x164
	WORD wWeapSel;						//0x168
	WORD wItemEffect;					//0x16A
	DWORD wItemCltEffect;				//0x16C
	DWORD dwSkPoints;					//0x170
	WORD wReqLevel;						//0x174
	WORD wReqStr;						//0x176
	WORD wReqDex;						//0x178
	WORD wReqInt;						//0x17A
	WORD wReqVit;						//0x17C
	WORD wReqSkill1;					//0x17E
	WORD wReqSkill2;					//0x180
	WORD wReqSkill3;					//0x182
	WORD wStartMana;					//0x184
	WORD wMinMana;						//0x186
	WORD wManaShift;					//0x188
	WORD wMana;							//0x18A
	WORD wLevelMana;					//0x18C
	BYTE nAttackRank;					//0x18E
	BYTE nLineOfSight;					//0x18F
	DWORD dwDelay;						//0x190
	DWORD wSkillDesc;					//0x194
	DWORD dwToHit;						//0x198
	DWORD dwLevToHit;					//0x19C
	DWORD dwToHitCalc;					//0x1A0
	BYTE nToHitShift;					//0x1A4
	BYTE nSrcDam;						//0x1A5
	WORD unk0x1A6;						//0x1A6
	DWORD dwMinDam;						//0x1A8
	DWORD dwMaxDam;						//0x1AC
	DWORD dwMinLevDam[5];				//0x1B0
	DWORD dwMaxLevDam[5];				//0x1C4
	DWORD dwDmgSymPerCalc;				//0x1D8
	WORD wEType;						//0x1DC
	WORD unk0x1DE;						//0x1DE
	DWORD dwEMin;						//0x1E0
	DWORD dwEMax;						//0x1E4
	DWORD dwEMinLev[5];					//0x1E8
	DWORD dwEMaxLev[5];					//0x1FC
	DWORD dwEDmgSymPerCalc;				//0x210
	DWORD dwELen;						//0x214
	DWORD dwELevLen[3];				//0x218
	DWORD dwELenSymPerCalc;				//0x224
	WORD wRestrict;						//0x228
	WORD wState[3];						//0x22A
	WORD wAiType;						//0x230
	WORD wAiBonus;						//0x232
	DWORD dwCostMult;					//0x234
	DWORD dwCostAdd;					//0x238
};

struct SkillDescTxt
{
	WORD wSkillDesc;               //0x00
	BYTE bSkillPage;               //0x02
	BYTE bSkillRow;                  //0x03
	BYTE bSkillColumn;               //0x04
	BYTE bListRow;                  //0x05
	BYTE bListPool;                  //0x06
	BYTE bIconCel;                  //0x07
	WORD wStrName;                  //0x08
	WORD wStrShort;                  //0x0A
	WORD wStrLong;                  //0x0C
	WORD wStrAlt;                  //0x0E
	WORD wStrMana;                  //0x10
	WORD wDescDam;                  //0x12
	WORD wDescAtt;                  //0x14
	WORD _1;					  //0x16
	DWORD dwDamCalc[2];               //0x18
	BYTE bPrgDamElem[4];            //0x20
	DWORD dwProgDmgMin[3];            //0x24
	DWORD dwProgDmgMax[3];            //0x30
	WORD wDescMissile[3];            //0x3C
	BYTE bDescLine[18];               //0x42
	WORD wDescTextA[17];            //0x54
	WORD wDescTextB[17];            //0x76
	DWORD dwDescCalcA[17];            //0x98
	DWORD dwDescCalcB[17];            //0xDC
};

struct D2CharItemStrc
{
	union
	{
		DWORD dwItemCode;				//0x00
		char szItemCode[4];				//0x00
	};
	char nBodyLoc;						//0x04
	char nItemCount;					//0x05
	WORD unk0x6;						//0x06
};

struct CharStatsTxt
{
	wchar_t wszName[16];				//0x00
	char szName[16];					//0x20
	BYTE nStr;							//0x30
	BYTE nDex;							//0x31
	BYTE nEne;							//0x32
	BYTE nVita;							//0x33
	BYTE nStamina;						//0x34
	BYTE nHpAdd;						//0x35
	BYTE nPercentStr;					//0x36
	BYTE nPercentInt;					//0x37
	BYTE nPercentDex;					//0x38
	BYTE nPercentVit;					//0x39
	WORD nManaRecovery;					//0x3A
	DWORD dwToHitFactor;				//0x3C
	BYTE nWalkSpeed;					//0x40
	BYTE nRunSpeed;						//0x41
	BYTE nRunDrain;						//0x42
	BYTE nLifePerLevel;					//0x43
	BYTE nStaminaPerLevel;				//0x44
	BYTE nManaPerLevel;					//0x45
	BYTE nLifePerVitality;				//0x46
	BYTE nStaminaPerVitality;			//0x47
	BYTE nManaPerMagic;					//0x48
	BYTE nBlockFactor;					//0x49
	WORD unk0x4A;						//0x4A
	DWORD dwWeaponClassCode;			//0x4C
	WORD nStatsPerLevel;				//0x50
	WORD wAllSkills;					//0x52
	WORD wSkillTab[3];					//0x54
	WORD wClassSkills;					//0x5A
	D2CharItemStrc pItems[10];			//0x5C
	WORD nStartSkill;					//0xAC
	WORD nBaseSkill[11];				//0xAE
};

struct ItemStatCostTxt  //size 0x144
{
	DWORD dwStat;				//0x000
	DWORD dwStatFlags;			//0x004
	BYTE nSendBits;				//0x008
	BYTE nSendParamBits;		//0x009
	BYTE nCSvBits;				//0x00A
	BYTE nCSvParam;				//0x00B
	DWORD dwDivide;				//0x00C
	DWORD dwMultiply;			//0x010
	DWORD dwAdd;				//0x014
	BYTE nValShift;				//0x018
	BYTE nSaveBits;				//0x019
	WORD nSaveBits09;			//0x01A
	DWORD dwSaveAdd;			//0x01C
	DWORD dwSaveAdd09;			//0x020
	BYTE nSaveParamBits;		//0x024
	BYTE __025[7];				//0x025
	DWORD dwMinAccr;			//0x02C
	WORD nEncode;				//0x030
	WORD nMaxStat;				//0x032
	WORD nDescPriority;			//0x034
	BYTE nDescFunc;				//0x036
	BYTE nDescVal;				//0x037
	WORD nDescStrPos;			//0x038
	WORD nDescStrNeg;			//0x03A
	WORD nDescStr2;				//0x03C
	WORD nDescGroup;			//0x03E
	BYTE nDescGrpFunc;			//0x040
	BYTE nDescGrpVal;			//0x041
	WORD nDescGrpStrPos;		//0x042
	WORD nDescGrpStrNeg;		//0x044
	WORD nDescGrpStr2;			//0x046
	WORD nItemEvent[2];			//0x048
	WORD nItemEventFunc[2];		//0x04C
	DWORD dwKeepZero;			//0x050
	BYTE nOp;					//0x054
	BYTE nOpParam;				//0x055
	WORD nOpBase;				//0x056
	WORD nOpStat[3];			//0x058
	BYTE __05E[226];			//0x05E
	DWORD dwStuff;				//0x140
};

struct UniqueItemsTxt
{
	WORD	uniqueId;			//0x00
	char    szIndex[32];		//0x02
	WORD	uniqueNameId;		//0x22
	DWORD	dwVersion;          //0x24
	union
	{
		DWORD dwCode;
		char szCode[4];   
	};							  //0x28
	DWORD dwFlags;				  //0x2C
	DWORD dwRarity;               //0x30
	WORD wLvl;                    //0x34
	WORD wLvlReq;                 //0x36
	BYTE nChrTransform;           //0x38
	BYTE nInvTransform;           //0x39
	char szFlippyFile[32];        //0x3A
	char szInvFile[34];           //0x5A
	DWORD dwCostMult;             //0x7C
	DWORD dwCostAdd;              //0x80
	WORD wDropSound;              //0x84
	WORD wUseSound;               //0x86
	DWORD dwDropSfxFrame;         //0x88   
	D2PropertyStrc hStats[12];    //0x90
};

struct SetItemsTxt
{
	WORD wSetItemId;               //0x00
	char szName[32];               //0x02
	WORD _1;	                   //0x22
	DWORD dwTblIndex;              //0x24
	union
	{
		DWORD dwCode;
		char szCode[4];
	};							   //0x28
	WORD nSet;	                   //0x2C
	WORD nSetItems;				   //0x2E
	WORD wLvl;                     //0x30
	WORD wLvlReq;                  //0x32
	DWORD dwRarity;                //0x34
	DWORD dwCostMult;              //0x38
	DWORD dwCostAdd;               //0x3C
	BYTE nChrTransform;            //0x40
	BYTE nInvTransform;            //0x41
	char szFlippyFile[32];         //0x42
	char szInvFile[32];            //0x62
	WORD wDropSound;               //0x82
	WORD wUseSound;                //0x84
	BYTE nDropSfxFrame;            //0x86
	BYTE nAddFunc;                 //0x87
	D2PropertyStrc hStats[9];        //0x88
	D2PropertyStrc hGreenStats[10];   //0x118
};

struct LevelsTxt
{
	WORD wLevelNo;                  //0x00
	BYTE nPal;                     //0x02
	BYTE nAct;                     //0x03
	BYTE nTeleport;                  //0x04
	BYTE nRain;                     //0x05
	BYTE nMud;                     //0x06
	BYTE nNoPer;                  //0x07
	BYTE nIsInside;                  //0x08
	BYTE nDrawEdges;               //0x09
	WORD unk0x0A;                  //0x0A
	DWORD dwWarpDist;               //0x0C
	WORD wMonLvl1;                  //0x10
	WORD wMonLvl2;                  //0x12
	WORD wMonLvl3;                  //0x14
	WORD wMonLvl1Ex;               //0x16
	WORD wMonLvl2Ex;               //0x18
	WORD wMonLvl3Ex;               //0x1A
	DWORD dwMonDen[3];               //0x1C
	BYTE nMonUMin[3];               //0x28
	BYTE nMonUMax[3];               //0x2B
	BYTE nMonWndr;                  //0x2E
	BYTE nMonSpcWalk;               //0x2F
	BYTE nQuest;                  //0x30
	BYTE nRangedSpawn;               //0x31
	DWORD dwNumMon;                  //0x32
	WORD wMon[25];                  //0x36
	WORD wNMon[25];                  //0x68
	WORD wUMon[25];                  //0x9A
	WORD wCMon[4];                  //0xCC
	WORD wCPct[4];                  //0xD4
	WORD wCAmt[4];                  //0xDC
	BYTE nWaypoint;                  //0xE4
	BYTE nObjGroup[8];               //0xE5
	BYTE nObjPrb[8];               //0xED
	char szLevelName[40];            //0xF5
	char szLevelWarp[40];            //0x11D
	char szEntryFile[41];            //0x145
	wchar_t wszLevelName[40];         //0x16E
	wchar_t wszLevelWarp[41];         //0x1BE
	DWORD dwThemes;                  //0x210
	DWORD dwFloorFilter;            //0x214
	DWORD dwBlankScreen;            //0x218
	DWORD dwSoundEnv;               //0x21C
};

struct DifficultyLevelsTxt //size 0x58
{
	DWORD ResistPenalty;			//0x00
	DWORD DeathExpPenalty;			//0x04
	DWORD UberCodeOddsNorm;			//0x08
	DWORD UberCodeOddsGood;			//0x0C
	DWORD MonsterSkillBonus;		//0x10
	DWORD MonsterFreezeDiv;			//0x14
	DWORD MonsterColdDiv;			//0x18
	DWORD AiCurseDiv;				//0x1C
	DWORD UltraCodeOddsNorm;		//0x20
	DWORD UltraCodeOddsGood;		//0x24
	DWORD LifeStealDiv;				//0x28
	DWORD ManaStealDiv;				//0x2C
	DWORD UniqueDmgBonus;			//0x30
	DWORD ChampionDmgBonus;			//0x34
	DWORD HireableBossDmgPercent;	//0x38
	DWORD MonsterCEDmgPercent;		//0x3C
	DWORD StaticFieldMin;			//0x40
	DWORD GambleRare;				//0x44
	DWORD GambleSet;				//0x48
	DWORD GambleUniq;				//0x4C
	DWORD GambleUber;				//0x50
	DWORD GambleUltra;				//0x54
};

struct ArenaTxt
{
	DWORD dwSuicide;
	DWORD dwPlayerKill;
	DWORD dwPKPercent;
	DWORD dwMonsterKill;
	DWORD dwPlayerDeath;
	DWORD dwPlayerDeathPercent;
	DWORD dwMonsterDeath;
};

struct CubeInputItem		//size (0x08)
{
	union 
	{
		WORD flag;			//+00
		struct 
		{
			WORD	byItemID : 1;				//"#item4Code" or "any"
			WORD	byItemTypeID : 1;			//"#itemType4Code"
			WORD	haveNoSocket : 1;			//",nos"
			WORD	haveSockets : 1;			//",sock"
			WORD	isEthereal : 1;				//",eth"
			WORD	isNotEthereal : 1;			//",noe"
			WORD	isSpecificItem : 1;			//"#itemUniqueName" or "#itemSetName" (set byItemID too)
			WORD	includeUpgradedVersions : 1;//",upg"
			WORD	isBasic : 1;				//",bas"
			WORD	isExceptional : 1;			//",exc"
			WORD	isElite : 1;				//",eli"
			WORD	isNotRuneword : 1;			//",nru"
		};
	};
	WORD	ID;					//0x02	FFFF = any items
	WORD	ItemID;				//0x04
	BYTE	Quality;			//0x06
	BYTE	Quantity;			//0x07
};

struct CubeOutputItem //size 0x54
{
	BYTE	bItemFlags;			//0x00
	BYTE	ItemType;			//0x01
	WORD	Item;				//0x02
	WORD	ItemID;				//0x04
	union
	{
		struct
		{
			BYTE nQuality;
			BYTE nQuantity;
		};
		struct
		{
			BYTE nLevel;
			BYTE nAct;
		};						   //for CUBE_OUTPUT_PORTAL
		WORD nParam;               //0x06
	};
	BYTE	Type;				//0x08
	BYTE	Lvl;				//0x09
	BYTE	PLvl;				//0x0A
	BYTE	ILvl;				//0x0B
	WORD	PrefixId[3];		//0x0C
	WORD	SuffixId[3];		//0x12
	struct 
	{							//size 0x0C
		DWORD	dwMod;				//0x00
		WORD	wModParam;			//0x04
		WORD	wModMin;			//0x06
		WORD	wModMax;			//0x08
		WORD	bModChance;			//0x0A
	} sMods[5]; 			//0x18	
};

struct CubeMainTxt		//size 0x148
{
	BYTE	bEnabled;			//0x00
	BYTE	bLadder;			//0x01
	BYTE	bMindiff;			//0x02
	BYTE	bClass;				//0x03
	BYTE	bOp;				//0x04
	BYTE	_1[3];				//0x05
	DWORD	dwParam;			//0x08
	DWORD	dwValue;			//0x0C
	WORD	bNuminputs;			//0x10
	WORD	wVersion;			//0x12
	CubeInputItem InputItem[7];	//0x14
	CubeOutputItem OutputItem[3];//0x4C
};

struct LvlMazeTxt
{
	DWORD dwLevelId;                //0x00
	DWORD dwRooms[3];               //0x04
	DWORD dwSizeX;                  //0x10
	DWORD dwSizeY;                  //0x14
	DWORD dwMerge;                  //0x18
};

struct ItemText
{
	wchar_t szName2[0x40];			//0x00
	union {
		DWORD dwCode;
		char szCode[4];
	};								//0x40
	BYTE _2[0x70];					//0x84
	WORD nLocaleTxtNo;				//0xF4
	BYTE _2a[0x19];					//0xF7
	BYTE xSize;						//0xFC
	BYTE ySize;						//0xFD
	BYTE _2b[13];					//0xFE
	BYTE nType;						//0x11E
	BYTE _3[0x0d];					//0x11F
	BYTE fQuest;					//0x12A
};

struct ItemsTxt //size = 0x1A8, Valid for Weapons, Armors, Misc.txts
{
	char	szflippyfile[32];		//0x00
	char	szinvfile[32];			//0x20
	char	szuniqueinvfile[32];	//0x40
	char	szsetinvfile[32];		//0x60
	union
	{
		DWORD	dwCode;				//0x80
		char	szCode[4];			//0x80
	};
	DWORD	dwNormCode;				//0x84
	DWORD	dwUberCode;				//0x88
	DWORD	dwUltraCode;			//0x8C
	DWORD	dwalternategfx;			//0x90
	DWORD	dwPSpell;				//0x94
	WORD	wState;					//0x98
	WORD	wcState1;				//0x9A
	WORD	wcState2;				//0x9C
	WORD	wStat1;					//0x9E
	WORD	wStat2;					//0xA0
	WORD	wStat3;					//0xA2
	DWORD	dwCalc1;				//0xA4
	DWORD	dwCalc2;				//0xA8
	DWORD	dwCalc3;				//0xAC
	DWORD	dwLen;					//0xB0
	WORD	bSpellDesc;				//0xB4
	WORD	wSpellDescStr;			//0xB6
	DWORD	dwSpellDescCalc;		//0xB8
	DWORD	dwBetterGem;			//0xBC
	DWORD	dwClass;				//0xC0
	DWORD	dw2handedwclass;		//0xC4
	DWORD	dwTMogType;				//0xC8
	DWORD	dwMinac;				//0xCC
	DWORD	dwMaxac;				//0xD0
	DWORD	dwGambleCost;			//0xD4
	DWORD	dwSpeed;				//0xD8
	DWORD	dwBitfield1;			//0xDC
	DWORD	dwcost;					//0xE0
	DWORD	dwminstack;				//0xE4
	DWORD	dwmaxstack;				//0xE8
	DWORD	dwspawnstack;			//0xEC
	DWORD	dwgemoffset;			//0xF0
	WORD	wnamestr;				//0xF4
	WORD	wversion;				//0xF6
	WORD	wautoprefix;			//0xF8
	WORD	wmissiletype;			//0xFA
	BYTE	brarity;				//0xFC
	BYTE	blevel;					//0xFD
	BYTE	bmindam;				//0xFE
	BYTE	bmaxdam;				//0xFF
	BYTE	bminmisdam;				//0x100
	BYTE	bmaxmisdam;				//0x101
	BYTE	b2handmindam;			//0x102
	BYTE	b2handmaxdam;			//0x103
	WORD	brangeadder;			//0x104
	WORD	wstrbonus;				//0x106
	WORD	wdexbonus;				//0x108
	WORD	wreqstr;				//0x10A
	WORD	wreqdex;				//0x10C
	BYTE	babsorbs;				//0x10E
	BYTE	binvwidth;				//0x10F
	BYTE	binvheight;				//0x110
	BYTE	bblock;					//0x111
	BYTE	bdurability;			//0x112
	BYTE	bnodurability;			//0x113
	BYTE	bmissile;				//0x114
	BYTE	bcomponent;				//0x115
	union
	{
		struct
		{
			BYTE	brArm;					//0x116
			BYTE	blArm;					//0x117
			BYTE	btorso;					//0x118
			BYTE	blegs;					//0x119
			BYTE	brspad;					//0x11A
			BYTE	blspad;					//0x11B
		};
		BYTE nArmorComp[6];
	};
	BYTE	b2handed;				//0x11C
	BYTE	buseable;				//0x11D
	WORD	wtype;					//0x11E
	WORD	wtype2;					//0x120
	WORD	bsubtype;				//0x122
	WORD	wdropsound;				//0x124
	WORD	wusesound;				//0x126
	BYTE	bdropsfxframe;			//0x128
	BYTE	bunique;				//0x129
	BYTE	bquest;					//0x12A
	BYTE	bquestdiffcheck;		//0x12B
	BYTE	btransparent;			//0x12C
	BYTE	btranstbl;				//0x12D
	BYTE 	_1;						//0x12E
	BYTE	blightradius;			//0x12F
	BYTE	bbelt;					//0x130
	BYTE	bAutobelt;				//0x131
	BYTE	bStackable;				//0x132
	BYTE	bSpawnable;				//0x133
	BYTE	bSpellIcon;				//0x134
	BYTE	bdurwarning;			//0x135
	BYTE	bqntwarning;			//0x136
	BYTE	bhasinv;				//0x137
	BYTE	bgemsockets;			//0x138
	BYTE	bTransmogrify;			//0x139
	BYTE	bTMogMin;				//0x13A
	BYTE	bTMogMax;				//0x13B
	BYTE	bhitclass;				//0x13C
	BYTE	b1or2handed;			//0x13D
	BYTE	bgemapplytype;			//0x13E
	BYTE	bLevelReq;				//0x13F
	BYTE	bmagiclvl;				//0x140
	BYTE	bTransform;				//0x141
	BYTE	bInvTrans;				//0x142
	BYTE	bcompactsave;			//0x143
	BYTE	bSkipName;				//0x144
	BYTE	bNameable;				//0x145
	BYTE	bAkaraMin;				//0x146
	BYTE	bGheedMin;				//0x147
	BYTE	bCharsiMin;				//0x148
	BYTE	bFaraMin;				//0x149
	BYTE	bLysanderMin;			//0x14A
	BYTE	bDrognanMin;			//0x14B
	BYTE	bHraltiMin;				//0x14C
	BYTE	bAlkorMin;				//0x14D
	BYTE	bOrmusMin;				//0x14E
	BYTE	bElzixMin;				//0x14F
	BYTE	bAshearaMin;			//0x150
	BYTE	bCainMin;				//0x151
	BYTE	bHalbuMin;				//0x152
	BYTE	bJamellaMin;			//0x153
	BYTE	bMalahMin;				//0x154
	BYTE	bLarzukMin;				//0x155
	BYTE	bDrehyaMin;				//0x156
	BYTE	bAkaraMax;				//0x157
	BYTE	bGheedMax;				//0x158
	BYTE	bCharsiMax;				//0x159
	BYTE	bFaraMax;				//0x15A
	BYTE	bLysanderMax;			//0x15B
	BYTE	bDrognanMax;			//0x15C
	BYTE	bHraltiMax;				//0x15D
	BYTE	bAlkorMax;				//0x15E
	BYTE	bOrmusMax;				//0x15F
	BYTE	bElzixMax;				//0x160
	BYTE	bAshearaMax;			//0x161
	BYTE	bCainMax;				//0x162
	BYTE	bHalbuMax;				//0x163
	BYTE	bJamellaMax;			//0x164
	BYTE	bMalahMax;				//0x165
	BYTE	bLarzukMax;				//0x166
	BYTE	bDrehyaMax;				//0x167
	BYTE	bAkaraMagicMin;			//0x168
	BYTE	bGheedMagicMin;			//0x169
	BYTE	bCharsiMagicMin;		//0x16A
	BYTE	bFaraMagicMin;			//0x16B
	BYTE	bLysanderMagicMin;		//0x16C
	BYTE	bDrognanMagicMin;		//0x16D
	BYTE	bHraltiMagicMin;		//0x16E
	BYTE	bAlkorMagicMin;			//0x16F
	BYTE	bOrmusMagicMin;			//0x170
	BYTE	bElzixMagicMin;			//0x171
	BYTE	bAshearaMagicMin;		//0x172
	BYTE	bCainMagicMin;			//0x173
	BYTE	bHalbuMagicMin;			//0x174
	BYTE	bJamellaMagicMin;		//0x175
	BYTE	bMalahMagicMin;			//0x176
	BYTE	bLarzukMagicMin;		//0x177
	BYTE	bDrehyaMagicMin;		//0x178
	BYTE	bAkaraMagicMax;			//0x179
	BYTE	bGheedMagicMax;			//0x17A
	BYTE	bCharsiMagicMax;		//0x17B
	BYTE	bFaraMagicMax;			//0x17C
	BYTE	bLysanderMagicMax;		//0x17D
	BYTE	bDrognanMagicMax;		//0x17E
	BYTE	bHraltiMagicMax;		//0x17F
	BYTE	bAlkorMagicMax;			//0x180
	BYTE	bOrmusMagicMax;			//0x181
	BYTE	bElzixMagicMax;			//0x182
	BYTE	bAshearaMagicMax;		//0x183
	BYTE	bCainMagicMax;			//0x184
	BYTE	bHalbuMagicMax;			//0x185
	BYTE	bJamellaMagicMax;		//0x186
	BYTE	bMalahMagicMax;			//0x187
	BYTE	bLarzukMagicMax;		//0x188
	BYTE	bDrehyaMagicMax;		//0x189
	BYTE	bAkaraMagicLvl;			//0x18A
	BYTE	bGheedMagicLvl;			//0x18B
	BYTE	bCharsiMagicLvl;		//0x18C
	BYTE	bFaraMagicLvl;			//0x18D
	BYTE	bLysanderMagicLvl;		//0x18E
	BYTE	bDrognanMagicLvl;		//0x18F
	BYTE	bHraltiMagicLvl;		//0x190
	BYTE	bAlkorMagicLvl;			//0x191
	BYTE	bOrmusMagicLvl;			//0x192
	BYTE	bElzixMagicLvl;			//0x193
	BYTE	bAshearaMagicLvl;		//0x194
	BYTE	bCainMagicLvl;			//0x195
	BYTE	bHalbuMagicLvl;			//0x196
	BYTE	bJamellaMagicLvl;		//0x197
	BYTE	bMalahMagicLvl;			//0x198
	BYTE	bLarzukMagicLvl;		//0x199
	WORD	bDrehyaMagicLvl;		//0x19A
	DWORD	dwNightmareUpgrade;		//0x19C
	DWORD	dwHellUpgrade;			//0x1A0
	BYTE	bPermStoreItem;			//0x1A4
	BYTE	bmultibuy;				//0x1A5
	WORD   _ALIGN;					//0x1A6
};

struct ObjectsTxt
{
	char szName[64];					//0x00
	wchar_t wszName[64];				//0x40
	char szToken[2];					//0xC0
	WORD wSpawnMax;						//0xC2
	BYTE nSelectable[8];				//0xC4
	DWORD dwTrapProb;					//0xCC
	DWORD dwSizeX;						//0xD0
	DWORD dwSizeY;						//0xD4
	BYTE pad0xD8;						//0xD8
	DWORD dwFrameCnt[7];				//0xD9
	WORD wFrameCnt7;					//0xF5
	BYTE pad0xF7;						//0xF7
	WORD wFrameDelta[8];				//0xF8
	BYTE nCycleAnim[8];					//0x108
	BYTE nLit[8];						//0x110
	BYTE nBlocksLight[8];				//0x118
	BYTE nHasCollision[8];				//0x120
	BYTE nIsAttackable0;				//0x128
	BYTE nStart[8];						//0x129
	BYTE nOrderFlag[8];					//0x131
	BYTE nEnvEffect;					//0x139
	BYTE nIsDoor;						//0x13A
	BYTE nBlocksVis;					//0x13B
	BYTE nOrientation;					//0x13C
	BYTE nPreOperate;					//0x13D
	BYTE nTrans;						//0x13E
	BYTE nMode[8];						//0x13F
	BYTE pad0x147;						//0x147
	DWORD dwXOffset;					//0x148
	DWORD dwYOffset;					//0x14C
	BYTE nDraw;							//0x150
	BYTE nHD;							//0x151
	BYTE nTR;							//0x152
	BYTE nLG;							//0x153
	BYTE nRA;							//0x154
	BYTE nLA;							//0x155
	BYTE nRH;							//0x156
	BYTE nLH;							//0x157
	BYTE nSH;							//0x158
	BYTE nS1;							//0x159
	BYTE nS2;							//0x15A
	BYTE nS3;							//0x15B
	BYTE nS4;							//0x15C
	BYTE nS5;							//0x15D
	BYTE nS6;							//0x15E
	BYTE nS7;							//0x15F
	BYTE nS8;							//0x160
	BYTE nTotalPieces;					//0x161
	BYTE nXSpace;						//0x162
	BYTE nYSpace;						//0x163
	BYTE nRed;							//0x164
	BYTE nGreen;						//0x165
	BYTE nBlue;							//0x166
	BYTE nSubClass;						//0x167
	DWORD dwNameOffset;					//0x168
	BYTE pad0x16C;						//0x16C
	BYTE nMonsterOK;					//0x16D
	BYTE nOperateRange;					//0x16E
	BYTE nShrineFunction;				//0x16F
	BYTE nAct;							//0x170
	BYTE nLockable;						//0x171
	BYTE nGore;							//0x172
	BYTE nRestore;						//0x173
	BYTE nRestoreVirgins;				//0x174
	WORD wSync;							//0x175
	BYTE pad0x177;						//0x177
	DWORD dwParm[8];					//0x178
	BYTE nTgtFX;						//0x198
	BYTE nTgtFY;						//0x199
	BYTE nTgtBX;						//0x19A
	BYTE nTgtBY;						//0x19B
	BYTE nDamage;						//0x19C
	BYTE nCollisionSubst;				//0x19D
	WORD pad0x19E;						//0x19E
	DWORD dwLeft;						//0x1A0
	DWORD dwTop;						//0x1A4
	DWORD dwWidth;						//0x1A8
	DWORD dwHeight;						//0x1AC
	BYTE nBeta;							//0x1B0
	BYTE nInitFn;						//0x1B1
	BYTE nPopulateFn;					//0x1B2
	BYTE nOperateFn;					//0x1B3
	BYTE nClientFn;						//0x1B4
	BYTE nOverlay;						//0x1B5
	BYTE nBlockMissile;					//0x1B6
	BYTE nDrawUnder;					//0x1B7
	DWORD dwOpenWarp;					//0x1B8
	DWORD dwAutomap;					//0x1BC
};

struct InventoryLayout //sizeof 0x14
{
	DWORD dwLeft;		//0x00
	DWORD dwRight;		//0x04
	DWORD dwTop;		//0x08
	DWORD dwBottom;		//0x0C
	union
	{
		struct
		{
			BYTE nGridX;//0x10
			BYTE nGridY;//0x11
		};
		struct
		{
			BYTE nWidth;//0x10
			BYTE nHeight;//0x11
		};
	};
	WORD _align;		 //0x12
};

struct InventoryLayout2
{
	BYTE SlotWidth;			//0x01
	BYTE SlotHeight;		//0x02
	BYTE unk1;				//0x03
	BYTE unk2;				//0x04
	DWORD Left;				//0x05
	DWORD Right;			//0x09
	DWORD Top;				//0x0D
	DWORD Bottom;			//0x11
	BYTE SlotPixelWidth;	//0x15
	BYTE SlotPixelHeight;	//0x19
};

struct InventoryTxt //sizeof 0xF0
{
	InventoryLayout Inventory;		//0x00
	InventoryLayout Grid;			//0x14
	union
		{
		struct 
		{
			InventoryLayout RightArm;		//0x28
			InventoryLayout Torso;			//0x3C
			InventoryLayout LeftArm;		//0x50
			InventoryLayout Head;			//0x64
			InventoryLayout Neck;			//0x78
			InventoryLayout RightHand;		//0x8C
			InventoryLayout LeftHand;		//0xA0
			InventoryLayout Belt;			//0xB4
			InventoryLayout Feet;			//0xC8
			InventoryLayout Gloves;			//0xDC
		};
		InventoryLayout hItem[9];
	};
};

struct AutoMagicTxt
{
	char szName[31];                //0x00
	BYTE _1;						//0x1F
	WORD wIndex;	                //0x20
	WORD wVersion;                  //0x22
	ItemsTxtStat hMods[3];          //0x24
	WORD wSpawnable;                //0x54
	WORD wTransformColor;           //0x56
	DWORD dwLevel;                  //0x58
	DWORD dwGroup;                  //0x5C
	DWORD dwMaxLevel;               //0x60
	BYTE nRare;                     //0x64
	BYTE nLevelReq;                 //0x65
	BYTE nClassSpecific;            //0x66
	BYTE nClass;                    //0x67
	WORD wClassLevelReq;            //0x68
	WORD wIType[7];                 //0x6A
	WORD wEType[5];                 //0x78
	BYTE bFrequency;                //0x82
	BYTE _2;						//0x83
	DWORD dwDivide;                 //0x84
	DWORD dwMultiply;               //0x88
	DWORD dwAdd;                    //0x8C
};

struct RunesTxt
{
	char szName[64];					 //0x00
	char szRuneName[64];                 //0x40
	BYTE bComplete;                      //0x80
	BYTE bServer;                        //0x81
	DWORD dwRwId;						 //0x82
	WORD dwIType[6];                     //0x86
	WORD dwEType[3];                     //0x92
	DWORD dwRune[6];                     //0x98
	ItemsTxtStat hStats[7];              //0xB0
};

struct LevelTxt
{
	WORD wLevelNo;						//0x00
	BYTE nPal;							//0x02
	BYTE nAct;							//0x03
	BYTE nTeleport;						//0x04
	BYTE nRain;							//0x05
	BYTE nMud;							//0x06
	BYTE nNoPer;						//0x07
	BYTE nIsInside;						//0x08
	BYTE nDrawEdges;					//0x09
	WORD unk0x0A;						//0x0A
	DWORD dwWarpDist;					//0x0C
	WORD wMonLvl1;						//0x10
	WORD wMonLvl2;						//0x12
	WORD wMonLvl3;						//0x14
	WORD wMonLvl1Ex;					//0x16
	WORD wMonLvl2Ex;					//0x18
	WORD wMonLvl3Ex;					//0x1A
	DWORD dwMonDen[3];					//0x1C
	BYTE nMonUMin[3];					//0x28
	BYTE nMonUMax[3];					//0x2B
	BYTE nMonWndr;						//0x2E
	BYTE nMonSpcWalk;					//0x2F
	BYTE nQuest;						//0x30
	BYTE nRangedSpawn;					//0x31
	DWORD dwNumMon;						//0x32
	WORD wMon[25];						//0x36
	WORD wNMon[25];						//0x68
	WORD wUMon[25];						//0x9A
	WORD wCMon[4];						//0xCC
	WORD wCPct[4];						//0xD4
	WORD wCAmt[4];						//0xDC
	BYTE nWaypoint;						//0xE4
	BYTE nObjGroup[8];					//0xE5
	BYTE nObjPrb[8];					//0xED
	char szLevelName[40];				//0xF5
	char szLevelWarp[40];				//0x11D
	char szEntryFile[41];				//0x145
	wchar_t wszLevelName[40];			//0x16E
	wchar_t wszLevelWarp[41];			//0x1BE
	DWORD dwThemes;						//0x210
	DWORD dwFloorFilter;				//0x214
	DWORD dwBlankScreen;				//0x218
	DWORD dwSoundEnv;					//0x21C
};

struct GemsTxt
{
	char szName[32];					//0x00
	char szLetter[8];					//0x20
	BYTE unk0x28[6];					//0x28
	BYTE nNumMods;						//0x2E
	BYTE nTransForm;					//0x2F
	DWORD dwWeaponMod1Code;				//0x30
	DWORD dwWeaponMod1Param;			//0x34
	DWORD dwWeaponMod1Min;				//0x38
	DWORD dwWeaponMod1Max;				//0x3C
	DWORD dwWeaponMod2Code;				//0x40
	DWORD dwWeaponMod2Param;			//0x44
	DWORD dwWeaponMod2Min;				//0x48
	DWORD dwWeaponMod2Max;				//0x4C
	DWORD dwWeaponMod3Code;				//0x50
	DWORD dwWeaponMod3Param;			//0x54
	DWORD dwWeaponMod3Min;				//0x58
	DWORD dwWeaponMod3Max;				//0x5C
	DWORD dwHelmMod1Code;				//0x60
	DWORD dwHelmMod1Param;				//0x64
	DWORD dwHelmMod1Min;				//0x68
	DWORD dwHelmMod1Max;				//0x6C
	DWORD dwHelmMod2Code;				//0x70
	DWORD dwHelmMod2Param;				//0x74
	DWORD dwHelmMod2Min;				//0x78
	DWORD dwHelmMod2Max;				//0x7C
	DWORD dwHelmMod3Code;				//0x80
	DWORD dwHelmMod3Param;				//0x84
	DWORD dwHelmMod3Min;				//0x88
	DWORD dwHelmMod3Max;				//0x8C
	DWORD dwShieldMod1Code;				//0x90
	DWORD dwShieldMod1Param;			//0x94
	DWORD dwShieldMod1Min;				//0x98
	DWORD dwShieldMod1Max;				//0x9C
	DWORD dwShieldMod2Code;				//0xA0
	DWORD dwShieldMod2Param;			//0xA4
	DWORD dwShieldMod2Min;				//0xA8
	DWORD dwShieldMod2Max;				//0xAC
	DWORD dwShieldMod3Code;				//0xB0
	DWORD dwShieldMod3Param;			//0xB4
	DWORD dwShieldMod3Min;				//0xB8
	DWORD dwShieldMod3Max;				//0xBC
};

struct ItemTypesTxt
{
	char szCode[4];						//0x00
	WORD wEquiv1;						//0x04
	WORD wEquiv2;						//0x06
	BYTE nRepair;						//0x08
	BYTE nBody;							//0x09
	BYTE nBodyLoc1;						//0x0A
	BYTE nBodyLoc2;						//0x0B
	WORD wShoots;						//0x0C
	WORD wQuiver;						//0x0E
	BYTE nThrowable;					//0x10
	BYTE nReload;						//0x11
	BYTE nReEquip;						//0x12
	BYTE nAutoStack;					//0x13
	BYTE nMagic;						//0x14
	BYTE nRare;							//0x15
	BYTE nNormal;						//0x16
	BYTE nCharm;						//0x17
	BYTE nGem;							//0x18
	BYTE nBeltable;						//0x19
	BYTE nMaxSock1;						//0x1A
	BYTE nMaxSock25;					//0x1B
	BYTE nMaxSock40;					//0x1C
	BYTE nTreasureClass;				//0x1D
	BYTE nRarity;						//0x1E
	BYTE nStaffMods;					//0x1F
	BYTE nCostFormula;					//0x20
	BYTE nClass;						//0x21
	BYTE nStorePage;					//0x22
	BYTE nVarInvGfx;					//0x23
	char szInvGfx1[32];					//0x24
	char szInvGfx2[32];					//0x44
	char szInvGfx3[32];					//0x64
	char szInvGfx4[32];					//0x84
	char szInvGfx5[32];					//0xA4
	char szInvGfx6[32];					//0xC4
};

struct MonLvlTxt
{
	DWORD dwAC[3];		//0x00
	DWORD dwLAC[3];		//0x0C
	DWORD dwTH[3];		//0x18
	DWORD dwLTH[3];		//0x24
	DWORD dwHP[3];		//0x30
	DWORD dwLHP[3];		//0x3C
	DWORD dwDM[3];		//0x48
	DWORD dwLDM[3];		//0x54
	DWORD dwXP[3];		//0x60
	DWORD dwLXP[3];		//0x6C
};

struct HirelingTxt
{
	DWORD dwVersion;					//0x00
	DWORD dwId;							//0x04
	DWORD dwClass;						//0x08
	DWORD dwAct;						//0x0C
	DWORD dwDifficulty;					//0x10
	DWORD dwSeller;						//0x14
	DWORD dwGold;						//0x18
	DWORD dwLevel;						//0x1C
	DWORD dwExpPerLvl;					//0x20
	DWORD dwHitpoints;					//0x24
	DWORD dwHitpointsPerLvl;			//0x28
	DWORD dwDefense;					//0x2C
	DWORD dwDefensePerLvl;				//0x30
	DWORD dwStr;						//0x34
	DWORD dwStrPerLvl;					//0x38
	DWORD dwDex;						//0x3C
	DWORD dwDexPerLvl;					//0x40
	DWORD dwAttackRate;					//0x44
	DWORD dwAttackRatePerLvl;			//0x48
	DWORD dwShare;						//0x4C
	DWORD dwDmgMin;						//0x50
	DWORD dwDmgMax;						//0x54
	DWORD dwDmgPerLvl;					//0x58
	DWORD dwResist;						//0x5C
	DWORD dwResistPerLvl;				//0x60
	DWORD dwDefaultChance;				//0x64
	DWORD dwHead;						//0x68
	DWORD dwTorso;						//0x6C
	DWORD dwWeapon;						//0x70
	DWORD dwShield;						//0x74
	DWORD dwSkill[6];					//0x78
	DWORD dwChance[6];					//0x90
	DWORD dwChancePerLvl[6];			//0xA8
	BYTE nMode[6];						//0xC0
	BYTE nLevel[6];						//0xC6
	BYTE nLvlPerLvl[6];					//0xCC
	BYTE nHireDesc;						//0xD2
	char szNameFirst[32];				//0xD3
	char szNameLast[32];				//0xF3
	BYTE unk0x113;						//0x113
	WORD wNameFirst;					//0x114
	WORD wNameLast;						//0x116
};

struct Monstats2Txt
{
	WORD nId;					//0x000
	WORD __002;					//0x002
	DWORD dwFlags;				//0x004
	BYTE nSizeX;				//0x008
	BYTE nSizeY;				//0x009
	BYTE nSpawnCol;				//0x00A
	BYTE nHeight;				//0x00B
	BYTE nOverlayHeight;		//0x00C
	BYTE nPixHeight;			//0x00D
	BYTE nMeleeRng;				//0x00E
	BYTE __00F;					//0x00F
	union
	{
		DWORD dwBaseW;
		char szBaseW[4];
	};							//0x010
	BYTE nHitClass;				//0x014
	BYTE __015[211];			//0x015
	DWORD dwComponentFlags;		//0x014
	DWORD dwTotalPieces;		//0x0EC
	DWORD dwModeFlags;			//0x0F0
	BYTE nDirections[16];		//0x0F4
	WORD nMoveModeFlags;		//0x104
	WORD __106;					//0x106
	BYTE nInfernoLen;			//0x108
	BYTE nInfernoAnim;			//0x109
	BYTE nInfernoRollback;		//0x10A
	BYTE nResurrectMode;		//0x10B
	WORD nResurrectSkill;		//0x10C
	WORD nHtTop;				//0x10E
	WORD nHtLeft;				//0x110
	WORD nHtWidth;				//0x112
	WORD nHtHeight;				//0x114
	WORD __116;					//0x116
	DWORD dwAutomapCel;			//0x118
	BYTE nLocalBlood;			//0x11C
	BYTE nBleed;				//0x11D
	BYTE nLight;				//0x11E
	BYTE nLightRGB[3];			//0x11F
	BYTE nUTrans[3];			//0x122
	BYTE __125[3];				//0x125
	DWORD dwHeart;				//0x128
	DWORD dwBodyPart;			//0x12C
	DWORD dwRestore;			//0x130
};

struct TCExProb //sizeof 0xC
{
	union
	{
		struct
		{
			short nMagic;      // +00
			short nRare;       // +02
			short nSet;        // +04
			short nUnique;     // +06
			short nSuperior;   // +08
			short nNormal;     // +0A
		};
		short nProbabilites[6];
	};
};

struct TCExInfo //sizeof 0x1C
{
	DWORD nClassic;         // +00 - prob for classic
	DWORD nProb;            // +04 - proc for lod
	short nItem;            // +08 - hcidx of item (also idx of other tc etc)
	short nMul;             // +0A - also hcidx of unique (also used by ma and mg)
	WORD fFlags;            // +0C
	TCExProb pProbs;		// +0E
	short wUnk;             // +1A
};

struct TreasureClassTxt // total 736 bytes
{
	union
	{
		struct
		{
			char nTreasuremyspClass[32];
			int nPicks;
			short nGroup;
			short nLevel;
			short nMagic;
			short nRare;
			short nSet;
			short nUnique;
			int iPadding12;
			int nNoDrop;
			union
			{
				struct
				{
					char nItem1[64];
					char nItem2[64];
					char nItem3[64];
					char nItem4[64];
					char nItem5[64];
					char nItem6[64];
					char nItem7[64];
					char nItem8[64];
					char nItem9[64];
					char nItem10[64];
					int nProb1;
					int nProb2;
					int nProb3;
					int nProb4;
					int nProb5;
					int nProb6;
					int nProb7;
					int nProb8;
					int nProb9;
					int nProb10;
				};
				struct
				{
					char nItems[10][64];     // 0x038
					int nProbs[10];          // 0x2B8
				};
			};
		};

		struct
		{
			short       nGroup;      // +00
			short       nLevel;      // +02
			int         nTypes;      // +04 - how many items in the tc array
			DWORD       dwClassic;   // +08 - sum prob for classic
			DWORD       dwProb;      // +0C - sum prob for expansion  
			int         nPicks;      // +10 - set to 1 for picks equal 0
			int         nNoDrop;     // +14
			short       nUnused;     // +18
			TCExProb	pProbability;// +1A / +1C / +1E / +20 / +22 / +24
			short		nUnused2;    // +26
			TCExInfo*	pInfo;       // +28 - TcExInfo* holds the actual data and chance for all the items in 
		};
	};
};

struct ShrinesTxt
{
	DWORD dwShrine;				//0x00
	DWORD dwArg[2];				//0x04
	DWORD dwDurationInFrames;	//0x0C
	BYTE nResetTimeInMins;		//0x10
	BYTE nRarity;				//0x11
	char szViewName[32];		//0x12
	char szNiftyPhrase[128];	//0x32
	WORD nEffectClass;			//0xB2
	DWORD dwLevelMin;			//0xB4
};

struct TxtLinkNode
{
	char szText[32];				//0x00
	int nLinkIndex;					//0x20
	TxtLinkNode* pPrevious;	//0x24
	TxtLinkNode* pNext;		//0x28
};

struct LinkTable
{
	int nRecords;					//0x00
	DWORD dw1;						//0x04
	DWORD dw2;						//0x08
	TxtLinkNode* pFirstNode;	//0x0C
};

struct ExperienceTxt
{
	DWORD dwAmazon;						//0x00
	DWORD dwSorceress;					//0x04
	DWORD dwNecromancer;				//0x08
	DWORD dwPaladin;					//0x0C
	DWORD dwBarbarian;					//0x10
	DWORD dwDruid;						//0x14
	DWORD dwAssassin;					//0x18
	DWORD dwExpRatio;					//0x1C
};

struct SetsTxt
{
	WORD nIndex;						//0x000
	WORD nName;							//0x002
	DWORD dwVersion;					//0x004
	DWORD __008;						//0x008
	DWORD dwSetItems;					//0x00C
	D2PropertyStrc PProps[8];			//0x010
	D2PropertyStrc FProps[8];			//0x090
	SetItemsTxt* pSetItemRecords[6];	//0x110
};

struct PetTypeTxt
{
	DWORD dwHcIdx;			//0x00
	DWORD dwPetFlags;		//0x04
	WORD nGroup;			//0x08
	WORD nBaseMax;			//0x0A
	WORD nNameStr;			//0x0C
	BYTE nIconType;			//0x0E
	char szIcon[5][32];		//0x0F
	DWORD __0AF;			//0xAF
	WORD nClass[4];			//0xB2
	BYTE __0BA[37];			//0xBA
};

struct MonUModTxt
{
	//...
};

struct sgptDataTable
{
	BYTE*	pPlayerClass;			//0x00
	DWORD	dwPlayerClassRecords;	//0x04
	BodyLocsTxt*	pBodyLocs;		//0x08
	DWORD	dwBodyLocsRecords;		//0x0C
	BYTE*	pStorePage;				//0x10
	DWORD	dwStorePageRecords;		//0x14
	BYTE*	pElemTypes;				//0x18
	DWORD	dwElemTypesRecords;		//0x1C
	BYTE*	pHitClass;				//0x20
	DWORD	dwHitClassRecords;		//0x24
	BYTE*	pMonMode;				//0x28
	DWORD	dwMonModeTxt;			//0x2C
	BYTE*	pPlrMode;				//0x30
	DWORD	dwPlrModeRecords;		//0x34
	BYTE*	pSkillCalc;				//0x38
	DWORD	dwSkillCalcRecords;		//0x3C
	BYTE*	pSkillCalcCache;		//0x40
	DWORD	dwSkillCalcCacheRecs;	//0x44
	DWORD	dwSkillCalcCacheDelta;	//0x48
	BYTE*	pSkillDescCalcCache;	//0x4C
	DWORD	dwSkillDescCalcCacheRecs;//0x50
	DWORD	dwSkillDescCalcCacheDelta;//0x54
	BYTE*	pMissCalc;				//0x58
	DWORD	dwMissCalcRecords;		//0x5C
	BYTE*	pMissCalcCache;			//0x60
	DWORD	dwMissCalcCacheRecs;	//0x64
	DWORD	dwMissCalcCacheDelta;	//0x68
	BYTE*	pSkillCodes;			//0x6C
	DWORD	dwSkillCodesRecs;		//0x70
	BYTE*	pEvents;				//0x74
	DWORD	dwEventsRecs;			//0x78
	BYTE*	pCompCodes;				//0x7C
	DWORD	dwCompCodesRecs;		//0x80
	DWORD	dwCompCodes;			//0x84
	BYTE*	pMonAi;					//0x88
	DWORD	dwMonAiRecs;			//0x8C
	DWORD	dwMonAi;				//0x90
	BYTE*	pItem;					//0x94
	BYTE*	pItemCalcCache;			//0x98
	DWORD	dwItemCalcCacheRecs;	//0x9C
	DWORD	dwItemCalcCache;		//0xA0
	PropertiesTxt*	pPropertiesTxt;	//0xA4
	BYTE*	dwProperties;			//0xA8
	DWORD	dwProportiesRecs;		//0xAC
	BYTE*	pRunes;					//0xB0
	BYTE*	pHireDescs;				//0xB4
	DWORD	dwHireDescsRecs;		//0xB8
	StatesTxt*	pStatesTxt;			//0xBC
	DWORD	dwStatesTxtRecs;		//0xC0
	DWORD	dwStates;				//0xC4
	BYTE*	pStateMaskFirst;		//0xC8
	BYTE*	pStateMaskArr[40];		//0xCC
	BYTE*	pProgressiveStates;		//0x16C
	DWORD	dwProgressiveStatesRecs;//0x170
	BYTE*	pCurseStates;			//0x174
	DWORD	dwCurseStatesRecs;		//0x178
	BYTE*	pTransStates;			//0x17C
	DWORD	dwTransStatesRecs;		//0x180
	BYTE*	pActionStates;			//0x184
	DWORD	dwActionStatesRecs;		//0x188
	BYTE*	pColorStates;			//0x18C
	DWORD	dwColorStatesRecs;		//0x190
	BYTE*	pSoundCodes;			//0x194
	BYTE*	pSoundCodesTxt;			//0x198
	DWORD	dwSoundCodesRecs;		//0x19C
	HirelingTxt*	pHirelings;		//0x1A0
	DWORD	dwHirelingsRecs;		//0x1A4
	DWORD	HireNameStart[256];		//0x1A8
	DWORD	HireNameEnd[256];		//0x5A8
	D2NpcTxt* pNpcTables;			//0x9A8
	int nNpcRecords;				//0x9AC
	BYTE*	pColorsTxt;				//0x9B0
	BYTE*	pColors;				//0x9B4
	LinkTable*	pTreasureClassEx;	//0x9B8
	TreasureClassTxt*	pTreasureClassExTxt;//0x9BC
	DWORD	dwTreasureClassExRecs;	//0x9C0
	TreasureClassTxt*	pChestTreasureClassLst[45];	//0x9C4
	MonStatsTxt* pMonStatsTxt;		//0xA78
	BYTE*	pMonStats;				//0xA7C
	DWORD	dwMonStatsRecs;			//0xA80
	BYTE*	pMonSoundsTxt;			//0xA84
	BYTE*	pMonSounds;				//0xA88
	DWORD	dwMonSoundsRecs;		//0xA8C
	Monstats2Txt*	pMonStats2Txt;	//0xA90
	LinkTable*	pMonStats2Links;	//0xA94
	DWORD	dwMonStats2Recs;		//0xA98
	BYTE*	pMonPlaceTxt;			//0xA9C
	BYTE*	pMonPlace;				//0xAA0
	DWORD	dwMonPlaceRecs;			//0xAA4
	BYTE*	pMonPresetTxt;			//0xAA8
	BYTE*	pMonPresetActLst[5];	//0xAAC
	DWORD	dwMonPresetActRecs[5];	//0xAC0
	BYTE*	pSuperUniquesTxt;		//0xAD4
	BYTE*	pSuperUniques;			//0xAD8
	DWORD	dwSuperUniquesRecs;		//0xADC
	WORD	SuperUniqeIdxList[66];	//0xAE0
	MissilesTxt*	pMissilesTxt;	//0xB64
	BYTE*	pMissiles;				//0xB68
	DWORD	dwMissilesRecs;			//0xB6C
	MonLvlTxt*	pMonLvl;				//0xB70
	DWORD	dwMonLvlRecs;			//0xB74
	BYTE*	pMonSeqTxt;				//0xB78
	BYTE*	pMonSeq;				//0xB7C
	DWORD	dwMonSeqRecs;			//0xB80
	BYTE*	pMonSequences;			//0xB84
	DWORD	dwMonSequencesRecs;		//0xB88
	SkillDescTxt*	pSkillDescTxt;	//0xB8C
	BYTE*	pSkillDesc;				//0xB90
	DWORD	dwSkillDescRecs;		//0xB94
	SkillsTxt* pSkillsTxt;			//0xB98
	BYTE*	pSkills;				//0xB9C
	DWORD	dwSkillsRecs;			//0xBA0
	BYTE*	pPlayerSkillCount;		//0xBA4
	DWORD	dwPlayerSkillCount;		//0xBA8
	BYTE*	pPlayerSkillList;		//0xBAC
	DWORD	dwPassiveSkillCount;	//0xBB0
	BYTE*	pPasssiveSkillList;		//0xBB4
	BYTE*	pOverlayTxt;			//0xBB8
	BYTE*	pOverlay;				//0xBBC
	DWORD	dwOverlayRecs;			//0xBC0
	CharStatsTxt*	pCharStatsTxt;	//0xBC4
	DWORD	dwCharsStatsRecs;		//0xBC8
	ItemStatCostTxt*pItemStatCostTxt;//0xBCC
	BYTE*	pItemStatCost;			//0xBD0
	DWORD	dwItemStatCostRecs;		//0xBD4
	BYTE*	pDescStats;				//0xBD8
	DWORD	nDescStats;				//0xBDC
	BYTE*	pMonEquip;				//0xBE0
	DWORD	dwMonEquipRecs;			//0xBE4
	PetTypeTxt*	pPetTypesTxt;		//0xBE8
	BYTE*	pPetTypes;				//0xBEC
	DWORD	dwPetTypesRecs;			//0xBF0
	BYTE*	pItemsType;				//0xBF4
	BYTE*	pItemsTypeTxt;			//0xBF8
	DWORD	dwItemsTypeRecs;		//0xBFC
	DWORD	dwItemsTypeNesting;		//0xC00
	BYTE*	pItemsTypeNesting;		//0xC04
	BYTE*	pSets;					//0xC08
	SetsTxt*pSetsTxt;				//0xC0C
	DWORD	dwSetsRecs;				//0xC10
	BYTE*	pSetItems;				//0xC14
	SetItemsTxt* pSetItemsTxt;		//0xC18
	DWORD	dwSetItemsRecs;			//0xC1C
	BYTE*	pUniqueItems;			//0xC20
	UniqueItemsTxt*	pUniqueItemsTxt;//0xC24
	DWORD	dwUniqItemsRecs;		//0xC28
	BYTE*	pMonProp;				//0xC2C
	BYTE*	pMonPropTxt;			//0xC30
	DWORD	dwMonPropRecs;			//0xC34
	BYTE*	pMonType;				//0xC38
	BYTE*	pMonTypeTxt;			//0xC3C
	DWORD	dwMonTypeRecs;			//0xC40
	BYTE*	pMonTypeNesting;		//0xC44
	DWORD	dwMonTypeNestingRecs;	//0xC48
	BYTE*	pMonUMod;				//0xC4C
	MonUModTxt*	pMonUModTxt;		//0xC50
	DWORD	dwMonUModRecs;			//0xC54
	LevelsTxt*	pLevelsTxt;			//0xC58
	DWORD	dwLevelsRecs;			//0xC5C
	BYTE*	pLvlDefs;				//0xC60
	BYTE*	pLvlPrest;				//0xC64
	DWORD	dwLvlPrestRecs;			//0xC68
	DWORD	ItemStatCostStuff[2];	//0xC6C
	BYTE*	pAnimTables;			//0xC74
	ExperienceTxt*	pExperienceTxt;	//0xC78
	DifficultyLevelsTxt*pDiffLvlsTxt;//0xC7C
	DWORD	dwDiffLvlsRecs;			//0xC80
	BYTE*	pExpFieldD2;			//0xC84
	DWORD	ExpFieldData[10];		//0xC88
	BYTE*	pLvlSubExtra;			//0xCB0
	DWORD	dwLvlSubExtraRecs;		//0xCB4
	BYTE*	pCharTemp;				//0xCB8
	DWORD	dwCharTempRecs;			//0xCBC
	ArenaTxt*pArena;				//0xCC0
	BYTE*	pLvlTypes;				//0xCC4
	BYTE*	pWaypoints;				//0xCC8
	DWORD	dwWaypointsRecs;		//0xCCC
	DWORD	dwLvlTypes;				//0xCD0
	BYTE*	pLvlWarp;				//0xCD4
	DWORD	dwLvlWarpRecs;			//0xCD8
	LvlMazeTxt*	pLvlMaze;				//0xCDC
	DWORD	dwLvlMazeRecs;			//0xCE0
	BYTE*	pLvlSub;				//0xCE4
	DWORD	dwLvlSubRecs;			//0xCE8
	BYTE*	pLvlSubCache;			//0xCEC
	DWORD	_1[3];					//0xCF0
	BYTE*	pMapCache;				//0xCFC
	DWORD	dwMapCacheRecs;			//0xD00
	CubeMainTxt*	pCubeMain;		//0xD04
	DWORD	dwCubeMainRecs;			//0xD08
	BOOL	bWriteBinFiles;			//0xD0C
};

#pragma pack()
#endif