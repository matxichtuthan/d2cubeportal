#pragma once

#ifndef _D2PTRS_H
#define _D2PTRS_H

#undef DrawText
#undef DrawTextEx

#ifdef _MSC_VER // MS Compiler                                                                                                                                                                                                  ///
#define D2FUNC(DLL, NAME, RETURN, CONV, ARGS, OFFSET) typedef RETURN (CONV* DLL##_##NAME##_t) ARGS; __declspec(selectany) extern DLL##_##NAME##_t DLL##_##NAME = (DLL##_##NAME##_t)GetDllOffset(#DLL, DLLBASE_##DLL, OFFSET);   ///
#define D2VAR(DLL, NAME, TYPE, OFFSET) typedef TYPE DLL##_##NAME##_vt; __declspec(selectany) extern DLL##_##NAME##_vt * DLL##_##NAME = (DLL##_##NAME##_vt *)GetDllOffset(#DLL, DLLBASE_##DLL, OFFSET);                          ///
#define D2PTR(DLL, NAME, OFFSET) __declspec(selectany) extern DWORD NAME = GetDllOffset(#DLL, DLLBASE_##DLL, OFFSET);                                                                                                           ///
#else // GCC Compiler                                                                                                                                                                                                           ///
#define D2FUNC(DLL, NAME, RETURN, CONV, ARGS, OFFSET) typedef RETURN (CONV* DLL##_##NAME##_t) ARGS; DLL##_##NAME##_t DLL##_##NAME __attribute__((weak)) = (DLL##_##NAME##_t)GetDllOffset(#DLL, DLLBASE_##DLL, OFFSET);          ///
#define D2VAR(DLL, NAME, TYPE, OFFSET) typedef TYPE DLL##_##NAME##_vt; DLL##_##NAME##_vt * DLL##_##NAME __attribute__((weak)) = (DLL##_##NAME##_vt *)GetDllOffset(#DLL, DLLBASE_##DLL, OFFSET);                                 ///
#define D2PTR(DLL, NAME, OFFSET) DWORD NAME __attribute__((weak)) = GetDllOffset(#DLL, DLLBASE_##DLL, OFFSET);                                                                                                                  ///
#endif                                                                                                                                                                                                                          ///
extern DWORD __fastcall GetDllOffset(char* ModuleName, DWORD BaseAddress, int Offset);

D2FUNC(D2GFX, GetHwnd, HWND, __stdcall, (), 0x7FB0)

D2VAR(D2COMMON, sgptDataTables, sgptDataTable*, 0x99E1C)
D2FUNC(D2COMMON, GetItemIdx, ItemsTxt*, __stdcall, (DWORD ItemCode, int* Idx), 0x71960)
D2FUNC(D2COMMON, GetUnitRoom, Room1*, __stdcall, (UnitAny* ptUnit), 0x2FE10)//D2Common.#10331(D2UnitStrc* pUnit);
D2FUNC(D2COMMON, GetLevelNoByRoom, int, __stdcall, (Room1* ptRoom), 0x3C000)//D2Common.#10826(D2RoomStrc* pRoom);
D2FUNC(D2COMMON, SetAnimMode, void, __stdcall, (UnitAny* ptObject, DWORD value), 0x33920)//D2Common.#11090(D2UnitStrc* pUnit, int nAnimMode);
D2FUNC(D2COMMON, UpdateRoomFlags, DWORD, __stdcall, (Room1* ptRoom, DWORD zero), 0x3BC10)//D2Common.#10346(D2RoomStrc* pRoom, BOOL bA2);

D2VAR(D2GAME, CubePortalTable, TblPortal, 0xFA5E8)
D2FUNC(D2GAME, D2CreateUnit, UnitAny*, __fastcall, (DWORD type, DWORD id, DWORD x, DWORD y, Game* ptGame, Room1* ptRoom, DWORD uk1, DWORD uk2, DWORD uk3), 0xE1D90)
D2FUNC(D2GAME, CreateLinkedPortalInLevel, UnitAny*, __stdcall, (Game*, UnitAny*, int, int), 0xA22E0)//D2Game.0xA22E0(D2GameStrc* pGame, D2UnitStrc* pPortal, int nLevel, int nA4);

D2FUNC(FOG, GetBinTxtRowByText, int, __stdcall, (void* pLink, const char* szText, DWORD nColumn), 0x121A0)
D2FUNC(FOG, GetBinTxtIndex, int, __stdcall, (void* pLink, DWORD dwOrigin, DWORD _1), 0x11F00)
#endif