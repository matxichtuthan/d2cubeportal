#pragma once

#ifndef _D2_CUBE_H
#define _D2_CUBE_H

namespace Cube
{
	DWORD TransCode(const char* ptCode);
	int ValidateItemType(const char* szItemCode);
	int ValidateItemCode(const char* szItemCode);
	int IsUniqueItemName(const char* szName);
	int IsSetItemName(const char* szName);

	DWORD __stdcall HandlerOutput_STUB(Game* pGame, UnitAny* pPlayer, CubeOutputItem *pCubeOutput);
	DWORD __stdcall HandlerOutput(Game* pGame, UnitAny* pPlayer, CubeOutputItem *pCubeOutput);
	BOOL __fastcall HandlerOutput_TXT(const char *szText, CubeMainTxt *pTxt, int nOffset, int nPos, int nRow, int nColumn);

	void OnPatch();
}

#endif