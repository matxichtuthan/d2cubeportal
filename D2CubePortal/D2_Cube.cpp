#include "Main.h"

DWORD Cube::TransCode(const char* ptCode)
{
	DWORD ItemCode = 0;

	if (strlen(ptCode) == 3)
	{
		char NewStr[5];
		sprintf_s(NewStr, 5, "%s ", ptCode);
		ItemCode = TransCode(NewStr);
	}
	else ItemCode = (DWORD)(ptCode[3] << 24) + (DWORD)(ptCode[2] << 16) + (DWORD)(ptCode[1] << 8) + (DWORD)(ptCode[0]);

	return ItemCode;
}

int Cube::ValidateItemType(const char* szItemCode)
{
	DWORD code = TransCode(szItemCode);
	int idx = FOG_GetBinTxtIndex((*D2COMMON_sgptDataTables)->pItemsType, code, 0);
	if (idx > 0)
		return idx;
	return -1;
}

int Cube::ValidateItemCode(const char* szItemCode)
{
	DWORD code = TransCode(szItemCode);
	int idx;
	if (D2COMMON_GetItemIdx(code, &idx))
		return idx;
	return -1;
}

int Cube::IsUniqueItemName(const char* szName)
{
	int nTxtRow = FOG_GetBinTxtRowByText((*D2COMMON_sgptDataTables)->pUniqueItems, szName, 0);
	if (nTxtRow >= 0)
		return nTxtRow;
	return -1;
}

int Cube::IsSetItemName(const char* szName)
{
	int nTxtRow = FOG_GetBinTxtRowByText((*D2COMMON_sgptDataTables)->pSetItems, szName, 0);
	if (nTxtRow >= 0)
		return nTxtRow;
	return -1;
}

BOOL __fastcall Cube::HandlerOutput_TXT(const char *szText, CubeMainTxt *pTxt, int nOffset, int nPos, int nRow, int nColumn)
{
	if (!szText || !szText[0] || !pTxt)
		return FALSE;

	CubeOutputItem* pField = &pTxt->OutputItem[nOffset];
	regex cube_regexp("([\\w\' ]+)(?:=(\\w+))?,?");

	smatch match;

	for (string text(szText); regex_search(text, match, cube_regexp); text = match.suffix().str()) 
	{
		if (match.empty()) 
			return FALSE;

		if (match[1].length() == 0) 
			return FALSE;

		string keyword = match[1].str(); 

		if (!_stricmp(szText, "Cow Portal"))
			pField->Type = CUBEOUTPUT_COWPORTAL;
		else if (!_stricmp(szText, "Pandemonium Portal"))
			pField->Type = CUBEOUTPUT_UBERQUEST;
		else if (!_stricmp(szText, "Pandemonium Finale Portal"))
			pField->Type = CUBEOUTPUT_UBERQUEST_FINAL;
		else if (keyword == "portal")
		{
			string param = match[2].str();
			int id = atoi(param.c_str());
			if (id == 0)
				return FALSE;

			pField->Type = CUBEOUTPUT_PORTAL;
			pField->nLevel = id % 255;
		}
		else if (keyword == "act" && pField->Type == CUBEOUTPUT_PORTAL) 
		{
			string param = match[2].str();

			int id = atoi(param.c_str());
			if (id == 0)
				return FALSE;

			pField->nAct = id % 6; // Limit to 5 Acts
		}
		else if (keyword == "qty") 
		{
			string param = match[2].str();
			int id = atoi(param.c_str());
			if (id == 0)
				return FALSE;

			pField->nQuantity = id;
		}

		else if (keyword == "sock") 
		{
			string param = match[2].str();
			int socks = atoi(param.c_str());
			if (socks == 0)
				return FALSE;

			pField->bItemFlags |= 2;
			pField->nQuantity = socks;
		}
		else if (keyword == "usetype")		pField->Type = CUBEOUTPUT_USETYPE;
		else if (keyword == "useitem")		pField->Type = CUBEOUTPUT_USEITEM;
		else if (keyword == "low")			pField->nQuality = ITEM_LOW;
		else if (keyword == "nor")			pField->nQuality = ITEM_NORMAL;
		else if (keyword == "hiq")			pField->nQuality = ITEM_SUPERIOR;
		else if (keyword == "mag")			pField->nQuality = ITEM_MAGIC;
		else if (keyword == "set")			pField->nQuality = ITEM_SET;
		else if (keyword == "rar")			pField->nQuality = ITEM_RARE;
		else if (keyword == "uni")			pField->nQuality = ITEM_UNIQUE;
		else if (keyword == "crf")			pField->nQuality = ITEM_CRAFTED;
		else if (keyword == "tmp")			pField->nQuality = ITEM_TEMPERED;
		else if (keyword == "mod")			pField->bItemFlags |= 0x1;
		else if (keyword == "eth")			pField->bItemFlags |= 0x4;
		else if (keyword == "uns")			pField->bItemFlags |= 0x10;
		else if (keyword == "rem")			pField->bItemFlags |= 0x20;
		else if (keyword == "reg")			{ pField->bItemFlags |= 0x40; pField->Type = CUBEOUTPUT_USETYPE; }
		else if (keyword == "exc")			pField->bItemFlags |= 0x80;
		else if (keyword == "eli")			pField->ItemType |= 0x1;
		else if (keyword == "rep")			pField->ItemType |= 0x2;
		else if (keyword == "rch") 			pField->ItemType |= 0x4;
		else if (keyword == "pre") 
		{
			string param = match[2].str();
			int id = atoi(param.c_str());
			if (id == 0)
				return FALSE;
			for (int i = 0; i < 3; ++i) 
			{
				if (!pField->PrefixId[i]) 
				{
					pField->PrefixId[i] = id;
					break;
				}
			}
		}
		else if (keyword == "suf") 
		{
			string param = match[2].str();
			int id = atoi(param.c_str());
			if (id == 0)
				return FALSE;
			for (int i = 0; i < 3; ++i) 
			{
				if (!pField->SuffixId[i]) 
				{
					pField->SuffixId[i] = id;
					break;
				}
			}
		}
		else if (keyword.length() <= 4 && Cube::ValidateItemCode(keyword.c_str()) >= 0) 
		{ // Test if it's an item code
			pField->Item = Cube::ValidateItemCode(keyword.c_str());
			pField->Type = CUBEOUTPUT_ITEM;
		}
		else if (keyword.length() <= 4 && Cube::ValidateItemType(keyword.c_str()) >= 0)
		{ // Test if it's an item type
			pField->Item = Cube::ValidateItemType(keyword.c_str());
			pField->Type = CUBEOUTPUT_ITEMTYPE;
		}
		else if (keyword.length() > 4 && Cube::IsUniqueItemName(keyword.c_str()) >= 0) 
		{
			int nTxtIdx = Cube::IsUniqueItemName(keyword.c_str());
			UniqueItemsTxt* pTxt = &(*D2COMMON_sgptDataTables)->pUniqueItemsTxt[nTxtIdx];

			int code;
			D2COMMON_GetItemIdx(pTxt->dwCode, &code);

			pField->Item = code;
			pField->bItemFlags |= 0x8;
			pField->nQuality = ITEM_UNIQUE;
			pField->ItemID = nTxtIdx + 1; // Not sure intention of this
			pField->ILvl = (BYTE)pTxt->wLvl;
			pField->Type = CUBEOUTPUT_ITEM;
		}
		else if (keyword.length() > 4 && Cube::IsSetItemName(keyword.c_str()) >= 0) 
		{
			int nTxtIdx = Cube::IsSetItemName(keyword.c_str());
			SetItemsTxt* pTxt = &(*D2COMMON_sgptDataTables)->pSetItemsTxt[nTxtIdx];

			int code;
			D2COMMON_GetItemIdx(pTxt->dwCode, &code);

			pField->Item = code;
			pField->bItemFlags |= 0x8;
			pField->nQuality = ITEM_SET;
			pField->ItemID = nTxtIdx + 1; // Not sure intention of this
			pField->ILvl = (BYTE)pTxt->wLvl;
			pField->Type = CUBEOUTPUT_ITEM;
		}
		else 
		{
			return FALSE;
		}
	}

	return TRUE;
}

DWORD __stdcall Cube::HandlerOutput(Game* pGame, UnitAny* pPlayer, CubeOutputItem *pCubeOutput)
{
	if (pCubeOutput->Type == CUBEOUTPUT_COWPORTAL) 
	{
		return D2GAME_CubePortalTable[1].CallBack(pGame, pPlayer);
	}
	else if (pCubeOutput->Type == CUBEOUTPUT_UBERQUEST) 
	{
		return D2GAME_CubePortalTable[2].CallBack(pGame, pPlayer);
	}
	else if (pCubeOutput->Type == CUBEOUTPUT_UBERQUEST_FINAL) 
	{
		return D2GAME_CubePortalTable[3].CallBack(pGame, pPlayer);
	}
	else if (pCubeOutput->Type == CUBEOUTPUT_PORTAL)
	{
		if (pPlayer->dwAct != (pCubeOutput->nAct - 1))
			return 0;

		return OpenPortal(pGame, pPlayer, pCubeOutput->nLevel);
	}
	return -1;
}

NAKED DWORD __stdcall Cube::HandlerOutput_STUB(Game* pGame, UnitAny* pPlayer, CubeOutputItem *pCubeOutput)
{
	__asm
	{
		mov[esp + 14h + 4], esi // preserve item level
		push ebp
		push ebp // CubeOutputItem
		push [esp + 12Ch + 8 + 4] // pPlayer
		push [esp + 12Ch + 8 + 4] // PGame
		call Cube::HandlerOutput
		pop ebp 
		cmp eax, -1
		je invalid_func
		mov[esp + 38h + 4], eax // Store result of the function
	invalid_func:
		ret
	}
}

void Cube::OnPatch()
{
	Patch(JUMP, "D2Common.dll", 0x1C460, (DWORD)Cube::HandlerOutput_TXT, 5, "CubeMain.Txt output field compiler replacement");
	Patch(CALL, "D2Game.dll", 0x7240C, (DWORD)Cube::HandlerOutput_STUB, 49, "Sever Side Functins CubeManinOutPut");
}