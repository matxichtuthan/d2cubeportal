#pragma once

#ifndef _D2STRUCTS_H
#define _D2STRUCTS_H

#pragma pack(1)

struct Game;
struct UnitAny;
struct PlayerData;
struct MonsterData;
struct ObjectData;
struct MissileData;
struct ItemData;
struct Room1;
struct Level;

struct TblPortal
{
	DWORD(__fastcall* CallBack)(Game* ptGame, UnitAny* pPlayer);
};

struct Coord
{
	int nX;
	int nY;
};

struct D2Seed
{
	union
	{
		struct
		{
			DWORD dwLoSeed;		//0x00
			DWORD dwHiSeed;		//0x04
		};

		unsigned long long int qwSeed;//0x00
	};
};

struct Game
{

};

struct Room1
{

};

struct RoomTile
{

};

struct PresetUnit
{

};

struct Room2
{
	DWORD _1[2];			//0x00
	Room2** pRoom2Near;	//0x08
	DWORD _2[2];			//0x0C
	D2Seed hSeed;			//0x14
	DWORD _2b;				//0x1C
	struct
	{
		DWORD dwRoomNumber; //0x00
		DWORD* _1;			//0x04
		DWORD* pdwSubNumber;//0x08
	} *pType2Info;			//0x20 <- points to 0x70 struct if dwPresetType == 1, 0xF8 struct if == 2
	Room2* pRoom2Next;		//0x24
	DWORD dwRoomFlags;		//0x28
	DWORD dwRoomsNear;		//0x2C
	Room1* pRoom1;			//0x30
	DWORD dwPosX;			//0x34
	DWORD dwPosY;			//0x38
	DWORD dwSizeX;			//0x3C
	DWORD dwSizeY;			//0x40
	DWORD _3;				//0x44
	DWORD dwPresetType;		//0x48
	RoomTile* pRoomTiles;	//0x4C
	DWORD _4[2];			//0x50
	Level* pLevel;			//0x58
	PresetUnit* pPreset;	//0x5C
};

struct Act
{

};

struct LevelPreset
{

};

struct PresetData
{

};

struct WildernessData
{

};

struct ActMisc
{

};

struct Level
{
	DWORD dwDrlgType;				//0x00 1 - maze, 2 - preset, 3 - wilderness
	DWORD dwLevelFlags;				//0x04
	DWORD _1[2];					//0x08
	Room2* pRoom2First;				//0x10
	union
	{
		LvlMazeTxt* pMazeTxt;    		//     for dwDrlgType == 1 (RANDOM MAZE)
		PresetData* pPreset;			//     for dwDrlgType == 2 (PRESET MAP)
		WildernessData* pWilderness;	//     for dwDrlgType == 3 (RANDOM AREA WITH PRESET SIZE)
	}; // 0x14
	DWORD _2;						//0x18
	DWORD dwPosX;					//0x1C
	DWORD dwPosY;					//0x20
	DWORD dwSizeX;					//0x24
	DWORD dwSizeY;					//0x28
	DWORD _3[96];					//0x2C
	Level* pNextLevel;				//0x1AC
	DWORD _4;						//0x1B0
	ActMisc* pMisc;					//0x1B4
	DWORD _5[2];					//0x1B8
	DWORD dwLevelType;				//0x1C0
	D2Seed hSeed;					//0x1C4
	LevelPreset* pLevelPresets;		//0x1CC
	DWORD dwLevelNo;				//0x1D0
};

struct Path
{
	WORD xOffset;					//0x00
	WORD xPos;						//0x02
	WORD yOffset;					//0x04
	WORD yPos;						//0x06
	DWORD xUnknown;					//0x08  16 * (wInitX - wInitY) <- Mby AutomapX
	DWORD yUnknown;					//0x0C  8 * (wInitX + wInitY + 1) <- Mby AutoampY
	WORD xTarget;					//0x10
	WORD yTarget;					//0x12
	DWORD _2[2];					//0x14
	Room1* pRoom1;					//0x1C
};

struct StaticPath
{

};

struct ObjectPath
{

};

struct PlayerData {};
struct MonsterData {};
struct ObjectData 
{
	ObjectsTxt* pTxt;				//0x00
	union
	{
		DWORD dwFlags;

		struct
		{
			BYTE nPortalLevel;
			BYTE nPortalFlags;
			WORD nPortalLevelEx;
		};

		struct
		{
			WORD nShrine;
			WORD ___006;
		};

		struct
		{
			DWORD dwChestFlags;
		};
	};								// +04
	ShrinesTxt* pShrineRecord;		// +08
	DWORD		dwOperateGUID;		// +0C
	BOOL		bPermanent;			// +10
	DWORD		nUnk;				// +14
	Coord* pDestRoom;				// +18
	Coord* pDestPortal;				// +20
	char		szOwner[16];		// +28
};

struct MissileData {};
struct ItemData {};

struct UnitAny
{
	DWORD	dwType;					//+00
	union
	{
		DWORD	dwTxtFileNo;		//+04
		DWORD	dwGUID;
	};
	void* pMemPool;					//+08
	DWORD	dwUnitId;				//+0C		
	union
	{
		DWORD dwMode;               //Player, Monster, Object, Items
		DWORD dwCollideType;        //Missiles
	};								//+10
	union
	{
		PlayerData* pPlayerData;
		MonsterData* pMonsterData;
		ObjectData* pObjectData;
		MissileData* pMissileData;
		ItemData* pItemData;
	};								//+14
	DWORD	dwAct;					//+18
	Act* pAct;					//+1C
	D2Seed	Seed;					//+20
	DWORD	dwInitSeed;				//+28
	union
	{
		Path* pPath;
		StaticPath* pStaticPath;
		ObjectPath* pObjectPath;
	};								//+2C
};

#pragma pack()

#endif