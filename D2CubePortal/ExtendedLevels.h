#pragma once

#ifndef _EXTENDEDLEVELS_H
#define _EXTENDEDLEVELS_H

namespace ExtendedLevels
{
	BYTE __fastcall GetActByLevelNo_STUB1(DWORD nLevel);
	BYTE __fastcall GetActByLevelNo_STUB2(DWORD nLevel);
	BYTE __fastcall GetActByLevel(Level* pLevel);
	BYTE __fastcall GetActByRoom2(int _1, Room2* pRoom2);
	DWORD __stdcall GetActByLevelNo(DWORD nLevel);

	void OnPatch();
}

#endif