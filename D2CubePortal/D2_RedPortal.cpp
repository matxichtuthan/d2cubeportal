#include "Main.h"

D2PTR(D2GAME, FindFreeCoords_I, 0xE930)
NAKED Room1* __fastcall D2GAME_FindFreeCoords(Coord* Desired, Room1* ptRoom, Coord* Output, BOOL Unk)
{
	__asm
	{
		push esi
		mov esi, ecx
		push[esp + 12]
		push[esp + 12]
		push edx
		call FindFreeCoords_I
		pop esi
		ret 8
	}
}

BOOL __stdcall OpenPortal(Game *pGame, UnitAny *pUnit, int LevelId)
{
	if (!pGame || !pUnit) return FALSE;

	if (LevelId <= 0 || LevelId >= TXT_GetLevelsRecordCount())
		return FALSE;

	Room1* pRoom = D2COMMON_GetUnitRoom(pUnit);
	if (pRoom)
	{
		int aLvl = D2COMMON_GetLevelNoByRoom(pRoom);
		if (aLvl != LevelId)
		{
			Coord Pos = { pUnit->pPath->xPos, pUnit->pPath->yPos };
			Coord Out = { 0, 0 };
			Room1* aRoom = D2GAME_FindFreeCoords(&Pos, pUnit->pPath->pRoom1, &Out, 1);
			if (aRoom)
			{
				UnitAny* pObject = D2GAME_D2CreateUnit(UNIT_OBJECT, 60, Out.nX, Out.nY, pGame, aRoom, 1, 1, 0);
				if (!pObject || !pObject->pObjectData) return FALSE;

				pObject->pObjectData->nPortalLevel = (BYTE)LevelId;
				pObject->pObjectData->nPortalLevelEx = (WORD)LevelId;

				D2COMMON_SetAnimMode(pObject, OBJ_MODE_OPERATING);

				UnitAny* pWayback = D2GAME_CreateLinkedPortalInLevel(pGame, pObject, LevelId, 1);
				if (pWayback)
				{
					D2COMMON_UpdateRoomFlags(aRoom, 0);
					Room1* bRoom = D2COMMON_GetUnitRoom(pWayback);
					D2COMMON_UpdateRoomFlags(bRoom, 0);
					return TRUE;
				}
			}
		}
	}
	return FALSE;
}