#pragma once

#ifndef _D2CONSTANTS_H
#define _D2CONSTANTS_H

enum CubeOutputTypes
{
	CUBEOUTPUT_USETYPE = -1,
	CUBEOUTPUT_USEITEM = -2,
	CUBEOUTPUT_ITEMTYPE = -3,
	CUBEOUTPUT_ITEM = -4,
	CUBEOUTPUT_COWPORTAL = 1,
	CUBEOUTPUT_UBERQUEST = 2,
	CUBEOUTPUT_UBERQUEST_FINAL = 3,
	CUBEOUTPUT_PORTAL = 4,
};

enum ItemQuality
{
	ITEM_INVALID = 0,
	ITEM_LOW,
	ITEM_NORMAL,
	ITEM_SUPERIOR,
	ITEM_MAGIC,
	ITEM_SET,
	ITEM_RARE,
	ITEM_UNIQUE,
	ITEM_CRAFTED,
	ITEM_TEMPERED
};

enum UnitTypes
{
	UNIT_PLAYER = 0,				//0x00 Players
	UNIT_MONSTER,					//0x01 Monsters
	UNIT_OBJECT,					//0x02 Objects
	UNIT_MISSILE,					//0x03 Missiles
	UNIT_ITEM,						//0x04 Items
	UNIT_TILE,						//0x05 Tiles
};

enum ObjectMode
{
	OBJ_MODE_IDLE,			// Object idle
	OBJ_MODE_OPERATING,		// Object operating
	OBJ_MODE_OPENED,		// Object opened
	OBJ_MODE_SPECIAL1,		// Object special 1
	OBJ_MODE_SPECIAL2,		// Object special 2
	OBJ_MODE_SPECIAL3,		// Object special 3
	OBJ_MODE_SPECIAL4,		// Object special 4
	OBJ_MODE_SPECIAL5	 	// Object special 5
};

#endif