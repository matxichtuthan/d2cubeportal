#pragma once

#ifndef h_d2txtrecords_
#define h_d2txtrecords_

//========================================================================================//
__forceinline LevelsTxt* __fastcall TXT_GetLevelsRecord(int nRecord)
{
	sgptDataTable* pDataTables = *D2COMMON_sgptDataTables;
	if (nRecord < 0 || nRecord >= (int)pDataTables->dwLevelsRecs) return NULL;

	return &pDataTables->pLevelsTxt[nRecord];
}

__forceinline int __fastcall TXT_GetLevelsRecordCount()
{
	sgptDataTable* pDataTables = *D2COMMON_sgptDataTables;
	return pDataTables->dwLevelsRecs;
}
//========================================================================================//
#endif